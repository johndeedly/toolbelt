#!/usr/bin/env bash
find src -mindepth 1 -maxdepth 1 -type d -exec sh -c 'cd "{}"; [ -d bin/Release ] && rm -rf bin/Release; dotnet build -c Release' \;
