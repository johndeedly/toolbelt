#!/usr/bin/env bash
find src -mindepth 1 -maxdepth 1 -type d -exec sh -c 'cd "{}"; [ -d pub ] && rm -rf pub; dotnet publish -c release' \;
