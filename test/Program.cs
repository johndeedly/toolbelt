﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace toolbelt
{
    public class Program
    {
#pragma warning disable 0162
#pragma warning disable 0649
        class Data
        {
            public int x, y;

            public override string ToString()
            {
                return $"({x}, {y})";
            }
        }

        class TestConfig
        {
            public string Test;
            public int Number { get; set; }
        }
#pragma warning restore 0649

        public class TestClass
        {
            public int PropertyTest { get; set; }
        }

        public static void Main(string[] args)
        {
            Logging.MinimumLoggingLevel = LoggingLevel.Information;
            Logging.ConsoleMinimumLevel = LoggingLevel.Debug;
            Logging.Initialize();

            // 3 MiB
            ulong size = 3145728;
            Logging.Information("Binary size of 3 MiB: {@Size}", Mathematics.FormatSizeBinary(size));
            Logging.Information("Decimal size of 3 MiB: {@Size}", Mathematics.FormatSizeDecimal(size));

            // 3 MB
            size = 3000000;
            Logging.Information("Binary size of 3 MB: {@Size}", Mathematics.FormatSizeBinary(size));
            Logging.Information("Decimal size of 3 MB: {@Size}", Mathematics.FormatSizeDecimal(size));

            size = ulong.MaxValue;
            Logging.Information("Binary size of ulong.MaxValue: {@Size}", Mathematics.FormatSizeBinary(size));
            Logging.Information("Decimal size of ulong.MaxValue: {@Size}", Mathematics.FormatSizeDecimal(size));

            Logging.Information("This prints an error (binary, -1MB): {@Size}", Mathematics.FormatSizeBinary(-1000000));
            Logging.Information("This prints an error (decimal, -1MB): {@Size}", Mathematics.FormatSizeDecimal(-1000000));

            
            Action<string> original = delegate(string x) {
                Console.WriteLine($"PInvoking managed method: {x}");
            };

            original("original.");

            MethodInfo mi = original.GetType().GetMethod("Invoke");
            
            Delegate first = mi.ToDelegate(original);
            first.DynamicInvoke("testing...");

            Action<string> pinvoked = mi.ToDelegate<Action<string>>(original);
            pinvoked("works as intended.");

            var printf = NativeUtils.GetDelegateToNativeFunction<Func<string, int>>("libc", "printf");
            int res = printf("Test test test\n");
            Console.WriteLine($"Number of chars printed (including newline): {res}");
            Console.Out.Flush();

            using (IScriptingWrapper lua = ScriptingUtils.InitializeLua(true))
            {
                lua.OutputDataReceived += (str) =>
                {
                    Logging.Information("Lua output: {@Msg}", str);
                };
                lua.ErrorDataReceived += (t, str) =>
                {
                    Logging.Error("{@ErrorType}: {@Msg}", Enum.GetName<ScriptingErrorType>(t), str);
                };
                lua.ExecuteString("print('Hello World!')");
                lua.ExecuteString("Logging.Warning('Logging from Lua')");
                lua.RegisterClassType(typeof(TestClass));
                lua.ExecuteString("test=TestClass()");
                lua.ExecuteString("test.PropertyTest = 2");
                TestClass test = lua.GetVariable<TestClass>("test");
                Logging.Information("{@i}", test.PropertyTest);
                lua.ExecuteString("error('This is a test error')");
                lua.RegisterMethod("writeToLog", null, typeof(Logging).GetMethod("Information", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, new[] { typeof(string) }));
                lua.ExecuteString("writeToLog('This is awesome!')");
                string code = @"
function test(msg)
  Logging.Information(msg)
end

Tasks.setImmediate(function()
  test('Scheduler Task 1a')
  Tasks.switch()
  test('Scheduler Task 1b')
end)
Tasks.setImmediate(function()
  test('Scheduler Task 2a')
  Tasks.switch()
  test('Scheduler Task 2b')
end)
Tasks.setImmediate(function()
  test('Scheduler Task 3a')
  Tasks.switch()
  test('Scheduler Task 3b')
end)
Tasks.setImmediate(function()
  test('Scheduler Task 4a')
  Tasks.switch()
  test('Scheduler Task 4b')
end)
Tasks.setImmediate(function()
  test('Scheduler Task 5a')
  Tasks.switch()
  test('Scheduler Task 5b')
end)
Tasks.runUntilComplete()
Logging.Information('tasks completed')
";
                lua.ExecuteString(code);
            }

            return;

            // using (IScriptingWrapper ps = ScriptingUtils.InitializePowerShell(true))
            // {
            //     ps.OutputDataReceived += (str) =>
            //     {
            //         Logging.Information("PowerShell output: {@Msg}", str);
            //     };
            //     ps.ErrorDataReceived += (t, str) =>
            //     {
            //         Logging.Error("{@ErrorType}: {@Msg}", Enum.GetName<ScriptingErrorType>(t), str);
            //     };
            //     ps.ExecuteString("Write-Host 'Hello World!'");
            //     ps.ExecuteString("$a.X = $y");
            //     ps.RegisterMethod("writeToLog", null, typeof(Logging).GetMethod("Information", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, new[] { typeof(string) }));
            //     ps.ExecuteString("$writeToLog.Invoke('This is awesome!')");
            // }

            // using (IScriptingWrapper py = ScriptingUtils.InitializePython(true))
            // {
            //     py.OutputDataReceived += (str) =>
            //     {
            //         Logging.Information("Python output: {@Msg}", str);
            //     };
            //     py.ErrorDataReceived += (t, str) =>
            //     {
            //         Logging.Error("{@ErrorType}: {@Msg}", Enum.GetName<ScriptingErrorType>(t), str);
            //     };
            //     py.ExecuteString("sys.stdout.write('Hello world!')");
            //     py.ExecuteString("a.x.z = 3");
            //     py.RegisterMethod("writeToLog", null, typeof(Logging).GetMethod("Information", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, new[] { typeof(string) }));
            //     py.ExecuteString("writeToLog('This is awesome!')");
            // }

            // Action<string> logMe = typeof(Logging)
            //     .GetMethod("Information", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public, new[] { typeof(string) })
            //     .ToDelegate<Action<string>>();
            // logMe("Delegate Test");

            // if (!System.Diagnostics.Debugger.IsAttached)
            // {
            //     int res = ShellUtils.PopupWindow("Question", "Testing", "Yes", "No", "Cancel");
            //     Logging.Information("{@i}", res);

            //     res = ShellUtils.PopupErrorWindow("Question", "Testing 2", "3", "4", "5", "6");
            //     Logging.Information("{@i}", res);
            // }

            // var cfg = ConfigParser.ParseJsonConfigFile<TestConfig>();
            // ConfigParser.WriteJsonConfigFile<TestConfig>(cfg, "toolbelt.cfg.bak.json");
            // var cfg_bak = ConfigParser.ParseJsonConfigFile<TestConfig>("toolbelt.cfg.bak.json");
            // Logging.Information("{@b}", cfg.Number == cfg_bak.Number && cfg.Test == cfg_bak.Test);
            // ConfigParser.WriteJsonConfigFile<TestConfig>(cfg, "toolbelt.cfg.bak2.json", true);
            // var cfg_bak2 = ConfigParser.ParseJsonConfigFile<TestConfig>("toolbelt.cfg.bak2.json", true);
            // Logging.Information("{@b}", cfg.Number == cfg_bak2.Number && cfg.Test == cfg_bak2.Test);

            // var cfg2 = ConfigParser.ParseYamlConfigFile<TestConfig>();
            // ConfigParser.WriteYamlConfigFile<TestConfig>(cfg2, "toolbelt.cfg.bak.yml");
            // var cfg2_bak = ConfigParser.ParseYamlConfigFile<TestConfig>("toolbelt.cfg.bak.yml");
            // Logging.Information("{@b}", cfg2.Number == cfg2_bak.Number && cfg2.Test == cfg2_bak.Test);
            // ConfigParser.WriteYamlConfigFile<TestConfig>(cfg2, "toolbelt.cfg.bak2.yml", true);
            // var cfg2_bak2 = ConfigParser.ParseYamlConfigFile<TestConfig>("toolbelt.cfg.bak2.yml", true);
            // Logging.Information("{@b}", cfg2.Number == cfg2_bak2.Number && cfg2.Test == cfg2_bak2.Test);

            // string[] array = new string[] {
            //     "H1N1",
            //     "H",
            //     "GH1N12",
            //     "OS234Z",
            //     "OS31",
            //     "OS12XY"
            // };

            // var sorted = array.NumberedOrderBy();
            // Logging.Information(string.Join(", ", sorted));

            // array = new string[] {
            //     "netstandard1.0",
            //     "netstandard2.0",
            //     "netcore3.1",
            //     "net5.0",
            //     "net6.0"
            // };

            // sorted = array.NumberedOrderBy();
            // Logging.Information(string.Join(", ", sorted));

            // array = new string[] {
            //     "2.3.0",
            //     "2.3.4",
            //     "3.1.0",
            //     "7.10.3",
            //     "7.8.14"
            // };

            // sorted = array.NumberedOrderByDescending();
            // Logging.Information(string.Join(", ", sorted));

            //Random rnd = new Random();
            //
            //Directory.CreateDirectory("/tmp/queue");
            //FileBasedQueue<Data> queue = new FileBasedQueue<Data>("/tmp/queue");
            //Parallel.For(0, 1000, (i) =>
            //{
            //    queue.Enqueue(new Data
            //    {
            //        x = rnd.Next(0, 1000),
            //        y = rnd.Next(0, 1000)
            //    }).Await();
            //});

            //Console.WriteLine(queue.Count().Await());

            //int i = 0;
            //Data iter;
            //while ((iter = queue.Dequeue().Await()) != null)
            //{
            //    if (i++ < 10)
            //        Console.WriteLine(iter);
            //}

            // SkipList<int> list = new SkipList<int>();
            // for (int i = 0; i < 100000; i++)
            // {
            //     list.Add(rnd.Next(0, 100000));
            // }
            // Console.WriteLine(list.Count);
            // Console.WriteLine(string.Join(", ", list.Take(10)));

            // Console.WriteLine(ShellUtils.RunShellTextAsync("ls").GetAwaiter().GetResult());
            // byte[] data;
            // using (HttpClient web = new HttpClient())
            // {
            //     data = web.GetByteArrayAsync("https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png").GetAwaiter().GetResult();
            //     using (MemoryStream mem = new MemoryStream(data))
            //     {
            //         mem.Seek(0, SeekOrigin.Begin);
            //         Console.WriteLine(ShellUtils.RunShellTextAsync("viu", "-", inputStream: mem).GetAwaiter().GetResult());
            //     }
            // }

            JsonDocument doc = JsonDocument.Parse("[{\"x\":\"0x00\",\"y\":1},{\"x\":\"0x0F\",\"y\":2}]");
            //using (Stream s = new FileStream("/tmp/test.xlsx", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            //{
            //    dt.FromJson(doc.RootElement).ToOxml(s);
            //}
            using (Stream stdout = Console.OpenStandardOutput())
            {
                DataTable dt = new DataTable();
                using(var stream = new MemoryStream())
                {
                    dt.FromJson(doc.RootElement).ToCsv(stdout).ToJson(stdout).ToText(stdout);
                }
            }

            // using (DataTable dt = new DataTable())
            // using (FileStream fs = new FileStream("test.csv", FileMode.Open, FileAccess.Read))
            // using (FileStream fs2 = new FileStream("test2.csv", FileMode.Create, FileAccess.Write))
            // {
            //     dt.FromCsv(fs).ToCsv(fs2);
            // }

            // if (args.Length == 0)
            // {
            //     Console.WriteLine(Mathematics.FastSquareRoot(2));
            //     Console.WriteLine(Mathematics.FastSquareRoot(2.0));
            //     Console.WriteLine(Math.Sqrt(2.0));

            //     ShellUtils.RunChildAsync("--child").GetAwaiter().GetResult();
            // }
            // else
            // {
            //     for (int i = 0; i < args.Length; i++)
            //         Console.WriteLine(args[i]);
            //     SkipList<int> list = new SkipList<int>();
            //     Random rnd = new Random();
            //     for (int i = 0; i < 100000; i++)
            //     {
            //         list.Add(rnd.Next(0, 100000));
            //     }
            //     Console.WriteLine(list.Count);
            // }

            Logging.MinimumLoggingLevel = LoggingLevel.Debug;

            Logging.Information("basic stuff");
            Action act = () => Logging.Information("easy");
            act.OnThread().Await();
            Logging.Information("concatting");
            Task.Delay(2000).Concat(() => Logging.Information("nice")).Await();
            
            Logging.Information("legen, wait for it");
            using (Task t = Task.Delay(2000))
            {
                t.Concat((Action)delegate
                {
                    Logging.Information("dary");
                }).Await();
            }

            Logging.Information("the answer");
            using (Task t = Task.Delay(2000))
            {
                int x = t.Concat(delegate
                {
                    return 42;
                }).Await();
                Logging.Information("{@i}", x);
            }

            Logging.Information("waiting for result");
            int x2 = Task.Delay(2000).Concat(delegate
            {
                return 12;
            }).Await();
            Logging.Information("waited for: " + x2);

            Logging.Information("begin chain");
            int x3 = Task.Delay(2000).Concat(delegate
            {
                Logging.Information("return");
                return 12;
            }).Concat(() => Task.Delay(2000)).Concat(delegate (int obj)
            {
                Logging.Information("doubled");
                return obj * 2;
            }).Await();
            Logging.Information("result: " + x3);

            Logging.Information("Awaiting result in two second");
            Task.Delay(2000).Concat(() => Logging.Information("done")).Await(TimeSpan.FromSeconds(5), () => Logging.Error("timeout reached"));

            Logging.Information("Awaiting result with return value in two second");
            Task.Delay(2000).Concat(() => 42).Concat(() => Logging.Information("done")).Await(TimeSpan.FromSeconds(5), () => Logging.Error("timeout reached"));

            Logging.Information("Awaiting timeout in two seconds");
            Task.Delay(5000).Concat(() => Logging.Error("error")).Await(TimeSpan.FromSeconds(2), () => Logging.Information("timeout reached"));

            Logging.Information("Awaiting another timeout in two seconds");
            Task.Delay(5000).Concat(() => 42).Concat(() => Logging.Error("error")).Await(TimeSpan.FromSeconds(2), () => Logging.Information("timeout reached"));

            // ((Action)(() => Console.WriteLine("once"))).RunOnceAt().Await();
            // ((Action)(() => Console.WriteLine("periodic"))).RunPeriodicAt("* * * * * *", true).Await();

            Logging.MinimumLoggingLevel = LoggingLevel.Information;
        }
#pragma warning restore 0162
    }
}