using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;

namespace toolbelt
{
    public static class Mathematics
    {
        public static float FastInverseSquareRoot(float x)
        {
            Span<float> tmp = new[] { x };
            Span<uint> integer = MemoryMarshal.Cast<float, uint>(tmp);
            // http[://]www[.]lomont[.]org/papers/2003/InvSqrt.pdf
            integer[0] = 0x5f375a86 - (integer[0] >> 1);
            tmp[0] = tmp[0] * (1.5f - x * 0.5f * tmp[0] * tmp[0]);
            return tmp[0];
        }

        public static double FastInverseSquareRoot(double x)
        {
            Span<double> tmp = new[] { x };
            Span<ulong> integer = MemoryMarshal.Cast<double, ulong>(tmp);
            // http[://]www[.]lomont[.]org/papers/2003/InvSqrt.pdf
            integer[0] = 0x5fe6ec85e7de30da - (integer[0] >> 1);
            tmp[0] = tmp[0] * (1.5 - x * 0.5 * tmp[0] * tmp[0]);
            return tmp[0];
        }

        public static float FastSquareRoot(float x)
        {
            return x * FastInverseSquareRoot(x);
        }

        public static double FastSquareRoot(double x)
        {
            return x * FastInverseSquareRoot(x);
        }

        public static bool IsNumber(this object value)
        {
            Type type;
            if (value is Type)
                type = (Type)value;
            else
                type = value.GetType();
            
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            };
        }

        public static double CalculateEntropyFromStream(Stream stream)
        {
            long size = stream.Length - stream.Position;
            long[] counts = new long[256];
            byte[] buf = new byte[4096];
            int read;
            while ((read = stream.Read(buf, 0, buf.Length)) > 0)
            {
                for (int i = 0; i < read; i++)
                {
                    counts[buf[i]]++;
                }
            }

            double entropy = 0;
            for (int i = 0; i < counts.Length; i++)
            {
                if (counts[i] > 0)
                {
                    double probability = (double)counts[i] / size;
                    entropy += probability * Math.Log(probability, 2);
                }
            }
            return -entropy;
        }

        static readonly string units = "KMGTPE";
        static readonly string error = "NaN";
        
        public static string FormatSizeDecimal(long bytes, int decimals = 2)
        {
            if (bytes < 0)
                return error;
            return FormatSizeDecimal((ulong)bytes, decimals);
        }
        
        public static string FormatSizeDecimal(ulong bytes, int decimals = 2)
        {
            ulong unit = 1000;
            if (bytes < unit) { return $"{bytes} B"; }

            if (decimals < 0)
                decimals = 0;
            int exp = (int)(Math.Log(bytes) / Math.Log(unit));
            // ulong cannot be larger than exa
            return string.Format(new NumberFormatInfo
            {
                NumberDecimalDigits = decimals
            }, "{0:F} {1}B", bytes / Math.Pow(unit, exp), units[exp - 1]);
        }

        public static string FormatSizeBinary(long bytes, int decimals = 2)
        {
            if (bytes < 0)
                return error;
            return FormatSizeBinary((ulong)bytes, decimals);
        }

        public static string FormatSizeBinary(ulong bytes, int decimals = 2)
        {
            ulong unit = 1024;
            if (bytes < unit) { return $"{bytes} B"; }
            
            if (decimals < 0)
                decimals = 0;
            int exp = (int)(Math.Log(bytes) / Math.Log(unit));
            // ulong cannot be larger than exbi
            return string.Format(new NumberFormatInfo
            {
                NumberDecimalDigits = decimals
            }, "{0:F} {1}iB", bytes / Math.Pow(unit, exp), units[exp - 1]);
        }
    }
}