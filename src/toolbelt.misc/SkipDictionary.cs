using System;
using System.Collections.Generic;

namespace toolbelt
{
    public interface ISkipDictionary<TKey, T> : ISkipList<KeyItemPair<TKey, T>> where TKey : IComparable<TKey>
    {
        T this[TKey key] { get; set; }
        void Add(TKey key, T item);
        bool Contains(TKey key);
        void CopyKeysTo(TKey[] array, int arrayIndex);
        void CopyItemsTo(T[] array, int arrayIndex);
        T Get(TKey key);
        bool Remove(TKey key);
        bool TryGet(TKey key, out T item);
    }

    public class KeyItemPair<TKey, T> : Tuple<TKey, T>, IComparable<KeyItemPair<TKey, T>> where TKey : IComparable<TKey>
    {
        public KeyItemPair(TKey key = default(TKey), T item = default(T)) : base(key, item)
        { }

        public int CompareTo(KeyItemPair<TKey, T> other)
        {
            return Item1.CompareTo(other.Item1);
        }
    }

    public class SkipDictionary<TKey, T> : SkipList<KeyItemPair<TKey, T>>, ISkipDictionary<TKey, T> where TKey : IComparable<TKey>
    {
        public T this[TKey key]
        {
            get => Get(key);
            set => Add(key, value);
        }

        public SkipDictionary(int height = 32) : base(height)
        { }

        public override void Add(KeyItemPair<TKey, T> item)
        {
            int lvl = coinflip.Next(height);
            SkipNode add = new SkipNode(lvl + 1, item);
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = item.CompareTo(iter.Next[i].Item);
                    if (diff == 0)
                    {
                        iter.Next[i] = iter.Next[i].Next[i];
                        continue;
                    }
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
                if (i <= lvl)
                {
                    add.Next[i] = iter.Next[i];
                    iter.Next[i] = add;
                }
            }
        }

        public void Add(TKey key, T item)
        {
            int lvl = coinflip.Next(height);
            SkipNode add = new SkipNode(lvl + 1, new KeyItemPair<TKey, T>(key, item));
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = key.CompareTo(iter.Next[i].Item.Item1);
                    if (diff == 0)
                    {
                        iter.Next[i] = iter.Next[i].Next[i];
                        continue;
                    }
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
                if (i <= lvl)
                {
                    add.Next[i] = iter.Next[i];
                    iter.Next[i] = add;
                }
            }
        }

        public bool Contains(TKey key)
        {
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = key.CompareTo(iter.Next[i].Item.Item1);
                    if (diff == 0)
                        return true;
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            return false;
        }

        public void CopyKeysTo(TKey[] array, int arrayIndex)
        {
            foreach (var item in this)
                array[arrayIndex++] = item.Item1;
        }

        public void CopyItemsTo(T[] array, int arrayIndex)
        {
            foreach (var item in this)
                array[arrayIndex++] = item.Item2;
        }

        public T Get(TKey key)
        {
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = key.CompareTo(iter.Next[i].Item.Item1);
                    if (diff == 0)
                        return iter.Next[i].Item.Item2;
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            throw new KeyNotFoundException();
        }

        public bool Remove(TKey key)
        {
            SkipNode iter = head;
            bool found = false;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = key.CompareTo(iter.Next[i].Item.Item1);
                    if (diff == 0)
                    {
                        found = true;
                        iter.Next[i] = iter.Next[i].Next[i];
                        continue;
                    }
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            return found;
        }

        public bool TryGet(TKey key, out T item)
        {
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = key.CompareTo(iter.Next[i].Item.Item1);
                    if (diff == 0)
                    {
                        item = iter.Next[i].Item.Item2;
                        return true;
                    }
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            item = default(T);
            return false;
        }
    }
}