using System;
using System.Collections.Generic;

namespace toolbelt
{
    public class NumberedArrayComparer<T> : IComparer<IEnumerable<T>> where T : IComparable<T>
    {
        public int Compare(IEnumerable<T> x, IEnumerable<T> y)
        {
            using (var xenum = x.GetEnumerator())
            using (var yenum = y.GetEnumerator())
            {
                bool xok = xenum.MoveNext(), yok = yenum.MoveNext();
                while (xok && yok)
                {
                    long xnum, ynum;
                    int tmp;
                    if (long.TryParse(xenum.Current.ToString(), out xnum) &&
                        long.TryParse(yenum.Current.ToString(), out ynum))
                        tmp = xnum.CompareTo(ynum);
                    else
                        tmp = xenum.Current.CompareTo(yenum.Current);
                    if (tmp != 0)
                        return tmp;
                    xok = xenum.MoveNext();
                    yok = yenum.MoveNext();
                }
                if (xok && !yok)
                    return 1;
                else if (!xok && yok)
                    return -1;
                else
                    return 0;
            }
        }
    }
}