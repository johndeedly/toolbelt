using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace toolbelt
{
    public static class ListExtensions
    {
        public static IEnumerable<IReadOnlyList<T>> Partitioned<T>(this IEnumerable<T> source, int size)
        {
            return new PartitionedEnumerable<T>(source, size);
        }

        public class PartitionedEnumerable<T> : IEnumerable<IReadOnlyList<T>>
        {
            IEnumerable<T> _col;
            int _size;

            public PartitionedEnumerable(IEnumerable<T> col, int size)
            {
                _col = col;
                _size = size;
            }

            public IEnumerator<IReadOnlyList<T>> GetEnumerator()
            {
                return new PartitionEnumerator<T>(_col.GetEnumerator(), _size);
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }

        public class PartitionEnumerator<T> : IEnumerator<IReadOnlyList<T>>
        {
            public IReadOnlyList<T> Current { get; private set; }

            object IEnumerator.Current => Current;

            IEnumerator<T> _col;
            int _partitionSize;

            public PartitionEnumerator(IEnumerator<T> col, int partitionSize)
            {
                _col = col;
                _partitionSize = partitionSize;
            }

            public void Dispose()
            {
                _col?.Dispose();
                _col = null;
            }

            public bool MoveNext()
            {
                List<T> list = new List<T>(_partitionSize);
                while (_col.MoveNext())
                {
                    list.Add(_col.Current);
                    if (list.Count == _partitionSize)
                        break;
                }
                if (list.Count > 0)
                {
                    Current = list;
                    return true;
                }
                Current = null;
                return false;
            }

            public void Reset()
            {
                _col.Reset();
                Current = null;
            }
        }

        private static readonly NumberedArrayComparer<string> nac = new NumberedArrayComparer<string>();

        public static IEnumerable<string> NumberedOrderBy(this IEnumerable<string> source)
        {
            var keyed = source.Select(x => new { k = SplitAlpha(x), v = x })
                .OrderBy(g => g.k, nac)
                .Select(g => g.v);
            return keyed;
        }

        public static IEnumerable<string> NumberedOrderByDescending(this IEnumerable<string> source)
        {
            var keyed = source.Select(x => new { k = SplitAlpha(x), v = x })
                .OrderByDescending(g => g.k, nac)
                .Select(g => g.v);
            return keyed;
        }

        private static IEnumerable<string> SplitAlpha(string input)
        {
            var words = new List<StringBuilder> { new StringBuilder() };
            for (var i = 0; i < input.Length; i++)
            {
                words[words.Count - 1].Append(input[i]);
                if (i + 1 < input.Length && char.IsDigit(input[i]) != char.IsDigit(input[i + 1]))
                {
                    words.Add(new StringBuilder());
                }
            }
            return words.Select(x => x.ToString());
        }
    }
}