using System;
using System.Collections;
using System.Collections.Generic;

namespace toolbelt
{
    public interface ISkipList<T> : ICollection<T> where T : IComparable<T>
    { }

    public class SkipList<T> : ISkipList<T> where T : IComparable<T>
    {
        protected class SkipNode
        {
            public SkipNode[] Next;
            public T Item;

            public SkipNode(int height = 32, T item = default(T))
            {
                Next = new SkipNode[height];
                Item = item;
            }
        }

        protected readonly Random coinflip = new ProportionalRandom();
        protected SkipNode head;
        protected readonly int height;

        public int Count
        {
            get
            {
                int cnt = 0;
                foreach (var elem in this)
                    cnt++;
                return cnt;
            }
        }

        public bool IsReadOnly => false;

        public SkipList(int height = 32)
        {
            this.height = height;
            this.head = new SkipNode(height);
            Clear();
        }

        public virtual void Add(T item)
        {
            int lvl = coinflip.Next(height);
            SkipNode add = new SkipNode(lvl + 1, item);
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = item.CompareTo(iter.Next[i].Item);
                    if (diff <= 0)
                        break;
                    iter = iter.Next[i];
                }
                if (i <= lvl)
                {
                    add.Next[i] = iter.Next[i];
                    iter.Next[i] = add;
                }
            }
        }

        public void Clear()
        {
            for (int i = height - 1; i >= 0; i--)
                head.Next[i] = null;
        }

        public bool Contains(T item)
        {
            SkipNode iter = head;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = item.CompareTo(iter.Next[i].Item);
                    if (diff == 0)
                        return true;
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            foreach (var item in this)
                array[arrayIndex++] = item;
        }

        public IEnumerator<T> GetEnumerator()
        {
            SkipNode iter = head;
            while (iter.Next[0] != null)
            {
                yield return iter.Next[0].Item;
                iter = iter.Next[0];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Remove(T item)
        {
            SkipNode iter = head;
            bool found = false;
            for (int i = height - 1; i >= 0; i--)
            {
                while (iter.Next[i] != null)
                {
                    int diff = item.CompareTo(iter.Next[i].Item);
                    if (diff == 0)
                    {
                        found = true;
                        iter.Next[i] = iter.Next[i].Next[i];
                        continue;
                    }
                    if (diff < 0)
                        break;
                    iter = iter.Next[i];
                }
            }
            return found;
        }
    }
}