using System;
using System.IO;
using System.Reflection;
using System.Text.Json;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace toolbelt
{
    public static class ConfigParser
    {
        public static T ParseJsonConfigFile<T>(string filename = null, bool pascalCase = false)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            if (filename == null || !File.Exists(filename))
                filename = $"{asm.GetName().Name}.cfg.json";
            if (!File.Exists(filename))
                filename = $"{Path.GetDirectoryName(new Uri(asm.Location).LocalPath)}/{filename}";
            if (!File.Exists(filename))
                return default(T);
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                T cfg;
                if (pascalCase)
                {
                    cfg = JsonSerializer.Deserialize<T>(fs, new JsonSerializerOptions
                    {
                        IncludeFields = true
                    });
                }
                else
                {
                    cfg = JsonSerializer.Deserialize<T>(fs, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        IncludeFields = true
                    });
                }
                return cfg;
            }
        }

        public static void WriteJsonConfigFile<T>(T config, string filename = null, bool pascalCase = false)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            if (filename == null)
                filename = $"{asm.GetName().Name}.cfg.json";
            using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                if (pascalCase)
                {
                    JsonSerializer.Serialize<T>(fs, config, new JsonSerializerOptions
                    {
                        IncludeFields = true,
                        WriteIndented = true
                    });
                }
                else
                {
                    JsonSerializer.Serialize<T>(fs, config, new JsonSerializerOptions
                    {
                        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                        IncludeFields = true,
                        WriteIndented = true
                    });
                }
            }
        }

        public static T ParseYamlConfigFile<T>(string filename = null, bool pascalCase = false)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            if (filename == null || !File.Exists(filename))
                filename = $"{asm.GetName().Name}.cfg.yml";
            if (!File.Exists(filename))
                filename = $"{Path.GetDirectoryName(new Uri(asm.Location).LocalPath)}/{filename}";
            if (!File.Exists(filename))
                return default(T);
            using (var fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var sr = new StreamReader(fs))
            {
                T cfg;
                if (pascalCase)
                {
                    var deserializer = new DeserializerBuilder()
                        .WithNamingConvention(PascalCaseNamingConvention.Instance)
                        .Build();
                    cfg = deserializer.Deserialize<T>(sr);
                }
                else
                {
                    var deserializer = new DeserializerBuilder()
                        .WithNamingConvention(CamelCaseNamingConvention.Instance)
                        .Build();
                    cfg = deserializer.Deserialize<T>(sr);
                }
                return cfg;
            }
        }

        public static void WriteYamlConfigFile<T>(T config, string filename = null, bool pascalCase = false)
        {
            Assembly asm = Assembly.GetEntryAssembly();
            if (filename == null)
                filename = $"{asm.GetName().Name}.cfg.yml";
            using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
            using (var sw = new StreamWriter(fs))
            {
                if (pascalCase)
                {
                    var serializer = new SerializerBuilder()
                        .WithNamingConvention(PascalCaseNamingConvention.Instance)
                        .Build();
                    serializer.Serialize(sw, config, typeof(T));
                }
                else
                {
                    var serializer = new SerializerBuilder()
                        .WithNamingConvention(CamelCaseNamingConvention.Instance)
                        .Build();
                    serializer.Serialize(sw, config, typeof(T));
                }
            }
        }
    }
}