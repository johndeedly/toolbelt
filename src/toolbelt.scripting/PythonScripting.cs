using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Python.Runtime;

namespace toolbelt
{
    internal class PythonScriptingWrapper : IScriptingWrapper, IDisposable
    {
        Py.GILState gil;
        PyModule scope;
        Exception lastError;
        SysIOWriter outwriter;
        SysIOWriter errwriter;

        internal PythonScriptingWrapper(Py.GILState gil, PyModule scope)
        {
            this.gil = gil;
            this.scope = scope;
            this.lastError = null;
            this.outwriter = new SysIOWriter(this)
            {
                TextWriter = Console.Out
            };
            this.errwriter = new SysIOWriter(this)
            {
                TextWriter = Console.Error
            };
            dynamic sys = scope.Import("sys");
            sys.stdout = this.outwriter;
            sys.stderr = this.errwriter;
        }

        ~PythonScriptingWrapper()
        {
            Dispose();
        }

        public void Dispose()
        {
            outwriter.close();
            outwriter = null;
            errwriter.close();
            errwriter = null;
            scope?.Dispose();
            scope = null;
            gil?.Dispose();
            gil = null;
            lastError = null;
            PythonEngine.Shutdown();
        }

        public bool RedirectStreams { get; set; }

        public event Action<string> OutputDataReceived;
        public event Action<ScriptingErrorType, string> ErrorDataReceived;

        private void SetAndWriteError(Exception ex)
        {
            if (ex == null)
                return;
            lastError = ex;
            if (RedirectStreams)
            {
                ErrorDataReceived?.Invoke(ScriptingErrorType.Error, ex.ToString());
            }
            else
            {
                errwriter.write($"{ex}\n");
            }
        }

        public bool SetVariable<T>(string path, T value)
        {
            try
            {
                scope.Set(path, value);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetVariable(string path, object value)
        {
            try
            {
                scope.Set(path, value);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public T GetVariable<T>(string path)
        {
            try
            {
                T val = scope.Get<T>(path);
                return val;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return default(T);
            }
        }

        public object GetVariable(string path)
        {
            try
            {
                PyObject val = scope.Get(path);
                return val.As<dynamic>();
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool NewTable(string path)
        {
            try
            {
                PyDict dict = new PyDict();
                scope.Set(path, dict);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<T>(string path, IEnumerable<T> arr)
        {
            try
            {
                PyList list = new PyList();
                if (arr != null)
                {
                    foreach (var elem in arr)
                    {
                        list.Append(elem.ToPython());
                    }
                }
                scope.Set(path, list);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IEnumerable<object> arr)
        {
            try
            {
                PyList list = new PyList();
                if (arr != null)
                {
                    foreach (var elem in arr)
                    {
                        list.Append(elem.ToPython());
                    }
                }
                scope.Set(path, list);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<K, V>(string path, IDictionary<K, V> dict)
        {
            try
            {
                PyDict val = new PyDict();
                if (dict != null)
                {
                    foreach (var elem in dict)
                    {
                        if (elem.Value != null)
                            val.SetItem(elem.Key.ToPython(), elem.Value.ToPython());
                        else
                            val.SetItem(elem.Key.ToPython(), PyObject.None);
                    }
                }
                scope.Set(path, val);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IDictionary<object, object> dict)
        {
            try
            {
                PyDict val = new PyDict();
                if (dict != null)
                {
                    foreach (var elem in dict)
                    {
                        if (elem.Value != null)
                            val.SetItem(elem.Key.ToPython(), elem.Value.ToPython());
                        else
                            val.SetItem(elem.Key.ToPython(), PyObject.None);
                    }
                }
                scope.Set(path, val);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public IDictionary<K, V> GetTable<K, V>(string path)
        {
            try
            {
                Dictionary<K, V> val = new Dictionary<K, V>();
                PyDict dict = scope.Get<PyDict>(path);
                foreach (var key in dict.Keys())
                {
                    var elem = dict.GetItem(key);
                    val.Add(key.As<K>(), elem.As<V>());
                }
                return val;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public IDictionary<object, object> GetTable(string path)
        {
            try
            {
                Dictionary<object, object> val = new Dictionary<object, object>();
                PyDict dict = scope.Get<PyDict>(path);
                foreach (var key in dict.Keys())
                {
                    var elem = dict.GetItem(key);
                    val.Add(key.As<dynamic>(), elem.As<dynamic>());
                }
                return val;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool RegisterClassType(Type netType)
        {
            try
            {
                scope.Exec($"import clr; clr.AddReference('{netType.Assembly.FullName}')");
                scope.Exec($"from {netType.Namespace} import {netType.Name}");
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool RegisterStaticMethod(string path, MethodInfo method) => RegisterMethod(path, null, method);

        private static readonly Func<Type[], Type> MakeNewCustomDelegate = (Func<Type[], Type>)Delegate.CreateDelegate(typeof(Func<Type[], Type>), typeof(Expression).Assembly.GetType("System.Linq.Expressions.Compiler.DelegateHelpers").GetMethod("MakeNewCustomDelegate", BindingFlags.NonPublic | BindingFlags.Static));

        public bool RegisterMethod(string path, object target, MethodInfo method)
        {
            Delegate del = method.ToDelegate(target);
            scope.Set(path, del);
            return true;
        }

        public bool RegisterDelegateType(Type netType, Type engineType)
        {
            // nothing to do here
            return false;
        }

        public bool ExecuteFile(string fileName)
        {
            try
            {
                string txt = File.ReadAllText(fileName);
                scope.Exec(txt);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool ExecuteString(string chunk, string chunkName = "chunk")
        {
            try
            {
                scope.Exec(chunk);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public Exception GetLastError()
        {
            return lastError;
        }

        public class SysIOWriter
        {
            PythonScriptingWrapper wrapper;

            public TextWriter TextWriter { get; set; } = Console.Out;

            public SysIOWriter(PythonScriptingWrapper wrapper)
            {
                this.wrapper = wrapper;
            }

            public void write(String str)
            {
                if (wrapper.RedirectStreams && wrapper.OutputDataReceived != null)
                    wrapper.OutputDataReceived(str);
                else
                    TextWriter.Write(str);
            }

            public void writelines(String[] str)
            {
                if (wrapper.RedirectStreams && wrapper.OutputDataReceived != null)
                {
                    foreach (String line in str)
                    {
                        wrapper.OutputDataReceived(line);
                    }
                }
                else
                {
                    foreach (String line in str)
                    {
                        TextWriter.WriteLine(line);
                    }
                }
            }

            public void flush()
            {
                if (!wrapper.RedirectStreams)
                    TextWriter.Flush();
            }

            public void close()
            {
                if (!wrapper.RedirectStreams && TextWriter != Console.Out && TextWriter != Console.Error)
                    TextWriter.Close();
            }
        }
    }
}