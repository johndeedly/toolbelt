dotnet = {}

--[[
if dotnet.any(o) then
end
--]]
function dotnet.any(o)
  local e = o:GetEnumerator()
  local any = e:MoveNext()
  if e["Dispose"] then e:Dispose() end
  return any
end

--[[
for x in dotnet.concat(o1, o2) do
    x:foo()
end
--]]
function dotnet.concat(o1, o2)
  local e1 = o1:GetEnumerator()
  local e2 = o2:GetEnumerator()
  return function()
    if e1:MoveNext() then
      return e1.Current
    elseif e2:MoveNext() then
      return e2.Current
    else
      if e1["Dispose"] then e1:Dispose() end
      if e2["Dispose"] then e2:Dispose() end
    end
  end
end

--[[
for x in dotnet.each(o) do
    x:foo()
end
--]]
function dotnet.each(o)
  local e = o:GetEnumerator()
  return function()
    if e:MoveNext() then
      return e.Current
    else
      if e["Dispose"] then e:Dispose() end
    end
  end
end

--[[
x = dotnet.first(o)
--]]
function dotnet.first(o)
  local e = o:GetEnumerator()
  local cur = nil
  if e:MoveNext() then
    cur = e.Current
  end
  if e["Dispose"] then e:Dispose() end
  return cur
end

--[[
x = dotnet.nth(o, n)
--]]
function dotnet.nth(o, n)
  if n < 1 then return nil end
  local e = o:GetEnumerator()
  local i = 1
  local cur = nil
  while e:MoveNext() and i <= n do
    cur = e.Current
    i = i + 1
  end
  if i < n then cur = nil end
  if e["Dispose"] then e:Dispose() end
  return cur
end

--[[
x = dotnet.last(o)
--]]
function dotnet.last(o)
  local e = o:GetEnumerator()
  local cur = nil
  while e:MoveNext() do
    cur = e.Current
  end
  if e["Dispose"] then e:Dispose() end
  return cur
end

--[[
for x1, x2 in dotnet.zip(o1, o2) do
    x1:foo()
    x2:bar()
end
--]]
function dotnet.zip(o1, o2)
  local e1 = o1:GetEnumerator()
  local e2 = o2:GetEnumerator()
  return function()
    if e1:MoveNext() or e2:MoveNext() then
      return e1.Current, e2.Current
    else
      if e1["Dispose"] then e1:Dispose() end
      if e2["Dispose"] then e2:Dispose() end
    end
  end
end

--[[
cnt = dotnet.count(o)
--]]
function dotnet.count(o)
  local e = o:GetEnumerator()
  local cnt = 0
  local cur = nil
  while e:MoveNext() do
    cur = e.Current
    cnt = cnt + 1
  end
  if e["Dispose"] then e:Dispose() end
  return cnt
end
