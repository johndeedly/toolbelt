using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace toolbelt
{
    internal class PowerShellScriptingWrapper : IScriptingWrapper, IDisposable
    {
        Runspace runspace;
        PowerShell ps;
        Exception lastError;

        internal PowerShellScriptingWrapper(Runspace runspace, PowerShell ps)
        {
            this.runspace = runspace;
            this.ps = ps;
            this.lastError = null;
            ps.Streams.Debug.DataAdded += ConsumeStreamOutput;
            ps.Streams.Error.DataAdded += ConsumeStreamOutput;
            ps.Streams.Information.DataAdded += ConsumeStreamOutput;
            ps.Streams.Progress.DataAdded += ConsumeStreamOutput;
            ps.Streams.Verbose.DataAdded += ConsumeStreamOutput;
            ps.Streams.Warning.DataAdded += ConsumeStreamOutput;
        }

        ~PowerShellScriptingWrapper()
        {
            Dispose();
        }

        public void Dispose()
        {
            ps?.Dispose();
            ps = null;
            runspace?.Dispose();
            runspace = null;
            lastError = null;
        }

        public bool RedirectStreams { get; set; }

        public event Action<string> OutputDataReceived;
        public event Action<ScriptingErrorType, string> ErrorDataReceived;

        private void SetAndWriteError(Exception ex)
        {
            if (ex == null)
                return;
            lastError = ex;
            if (RedirectStreams)
            {
                Type t = ex.GetType();
                if (t.Namespace.StartsWith("System.Management.Automation"))
                    ErrorDataReceived?.Invoke(ScriptingErrorType.EngineError, $"[{t.Name}] {ex.Message}");
                else
                    ErrorDataReceived?.Invoke(ScriptingErrorType.Error, ex.ToString());
            }
            else
            {
                Console.Error.WriteLine(ex);
            }
        }

        public bool SetVariable<T>(string path, T value)
        {
            try
            {
                ps.Runspace.SessionStateProxy.SetVariable(path, value);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetVariable(string path, object value)
        {
            try
            {
                ps.Runspace.SessionStateProxy.SetVariable(path, value);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public T GetVariable<T>(string path)
        {
            try
            {
                object val = ps.Runspace.SessionStateProxy.GetVariable(path);
                if (val != null && typeof(T).IsAssignableFrom(val.GetType()))
                    return (T)val;
                return default(T);
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return default(T);
            }
        }

        public object GetVariable(string path)
        {
            try
            {
                object val = ps.Runspace.SessionStateProxy.GetVariable(path);
                return val;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool NewTable(string path)
        {
            try
            {
                ps.Runspace.SessionStateProxy.SetVariable(path, new DataTable());
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<T>(string path, IEnumerable<T> arr)
        {
            try
            {
                List<T> val = new List<T>();
                if (arr != null)
                    val.AddRange(arr);
                ps.Runspace.SessionStateProxy.SetVariable(path, val);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IEnumerable<object> arr)
        {
            try
            {
                List<object> val = new List<object>();
                if (arr != null)
                    val.AddRange(arr);
                ps.Runspace.SessionStateProxy.SetVariable(path, val);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<K, V>(string path, IDictionary<K, V> dict)
        {
            try
            {
                if (dict != null)
                {
                    ps.Runspace.SessionStateProxy.SetVariable(path, dict);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IDictionary<object, object> dict)
        {
            try
            {
                if (dict != null)
                {
                    ps.Runspace.SessionStateProxy.SetVariable(path, dict);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public IDictionary<K, V> GetTable<K, V>(string path)
        {
            try
            {
                object val = ps.Runspace.SessionStateProxy.GetVariable(path);
                return val as IDictionary<K, V>;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public IDictionary<object, object> GetTable(string path)
        {
            try
            {
                object val = ps.Runspace.SessionStateProxy.GetVariable(path);
                return val as IDictionary<object, object>;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool RegisterClassType(Type netType)
        {
            try
            {
                Assembly.Load(netType.Assembly.GetName());
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool RegisterStaticMethod(string path, MethodInfo method) => RegisterMethod(path, null, method);

        private static readonly Func<Type[], Type> MakeNewCustomDelegate = (Func<Type[], Type>)Delegate.CreateDelegate(typeof(Func<Type[], Type>), typeof(Expression).Assembly.GetType("System.Linq.Expressions.Compiler.DelegateHelpers").GetMethod("MakeNewCustomDelegate", BindingFlags.NonPublic | BindingFlags.Static));

        public bool RegisterMethod(string path, object target, MethodInfo method)
        {
            try
            {
                Delegate del = method.ToDelegate(target);
                ps.Runspace.SessionStateProxy.SetVariable(path, del);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool RegisterDelegateType(Type netType, Type engineType)
        {
            // nothing to do here
            return false;
        }

        public bool ExecuteFile(string fileName)
        {
            try
            {
                string data = File.ReadAllText(fileName);
                ps.Commands.Clear();
                ps.AddScript(data).Invoke();
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool ExecuteString(string chunk, string chunkName = "chunk")
        {
            try
            {
                ps.Commands.Clear();
                ps.AddScript(chunk).Invoke();
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public Exception GetLastError()
        {
            return lastError;
        }

        private void ConsumeStreamOutput(object s, DataAddedEventArgs e)
        {
            var data = ((IList)s)[e.Index];
            switch (data)
            {
                case ErrorRecord er:
                    if (RedirectStreams)
                    {
                        if (er.Exception != null)
                        {
                            ErrorDataReceived?.Invoke(ScriptingErrorType.ScriptError, $"[{er.Exception.GetType().Name} in {er.ScriptStackTrace.Split(": ", StringSplitOptions.TrimEntries).Last()}] {er.Exception.Message}");
                        }
                        else if (er.ErrorDetails != null)
                        {
                            ErrorDataReceived?.Invoke(ScriptingErrorType.ScriptError, $"[{er.ScriptStackTrace.Split(": ", StringSplitOptions.TrimEntries).Last()}] {er.Exception.Message}");
                        }
                        else
                        {
                            ErrorDataReceived?.Invoke(ScriptingErrorType.ScriptError, er.ToString());
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine(er);
                    }
                    break;
                default:
                    if (RedirectStreams)
                    {
                        OutputDataReceived?.Invoke(data.ToString());
                    }
                    else
                    {
                        Console.Out.WriteLine(data);
                    }
                    break;
            }
        }
    }
}