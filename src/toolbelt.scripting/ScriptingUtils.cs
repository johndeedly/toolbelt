using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Reflection;
using System.Text;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using NLua;
using Python.Runtime;

namespace toolbelt
{
    public interface IScriptingWrapper : IDisposable
    {
        bool RedirectStreams { get; set; }

        event Action<string> OutputDataReceived;
        event Action<ScriptingErrorType, string> ErrorDataReceived;

        bool SetVariable<T>(string path, T value);
        bool SetVariable(string path, object value);
        T GetVariable<T>(string path);
        object GetVariable(string path);
        bool NewTable(string path);
        bool SetTable<T>(string path, IEnumerable<T> arr);
        bool SetTable(string path, IEnumerable<object> arr);
        bool SetTable<K, V>(string path, IDictionary<K, V> dict);
        bool SetTable(string path, IDictionary<object, object> dict);
        IDictionary<K, V> GetTable<K, V>(string path);
        IDictionary<object, object> GetTable(string path);
        bool RegisterClassType(Type netType);
        bool RegisterStaticMethod(string path, MethodInfo method);
        bool RegisterMethod(string path, object target, MethodInfo method);
        bool RegisterDelegateType(Type netType, Type engineType);
        bool ExecuteFile(string fileName);
        bool ExecuteString(string chunk, string chunkName = "chunk");
        Exception GetLastError();
    }

    public enum ScriptingErrorType
    {
        ScriptError,
        EngineError,
        Error
    }

    public static class ScriptingUtils
    {
        public static IScriptingWrapper InitializeLua(bool redirectStreams = false, Encoding encoding = null)
        {
            Lua lua = new Lua();
            lua.State.Encoding = encoding ?? new UTF8Encoding(false);
            LuaScriptingWrapper wrapper = new LuaScriptingWrapper(lua)
            {
                RedirectStreams = redirectStreams
            };

            wrapper.RegisterClassType(typeof(Logging));
            
            using (StreamReader sr = new StreamReader(typeof(LuaScriptingWrapper).Assembly.GetManifestResourceStream("toolbelt.scripting.dotnet.lua")))
            {
                string content = sr.ReadToEnd();
                wrapper.ExecuteString(content, "dotnet.lua");
            }
            using (StreamReader sr = new StreamReader(typeof(LuaScriptingWrapper).Assembly.GetManifestResourceStream("toolbelt.scripting.Tasks.lua")))
            {
                string content = sr.ReadToEnd();
                wrapper.ExecuteString(content, "Tasks.lua");
            }
            wrapper.RegisterStaticMethod("dotnet.castenum", typeof(ScriptingUtils).GetMethod(nameof(castEnumerable), BindingFlags.Static | BindingFlags.NonPublic));
            wrapper.RegisterStaticMethod("dotnet.castdict", typeof(ScriptingUtils).GetMethod(nameof(castDictionary), BindingFlags.Static | BindingFlags.NonPublic));
            wrapper.RegisterMethod("dotnet.setenum", wrapper, typeof(LuaScriptingWrapper).GetMethod("SetTable", 0, new[] { typeof(string), typeof(IEnumerable<object>) }));
            wrapper.RegisterMethod("dotnet.setdict", wrapper, typeof(LuaScriptingWrapper).GetMethod("SetTable", 0, new[] { typeof(string), typeof(IDictionary<object, object>) }));
            wrapper.RegisterMethod("dotnet.getdict", wrapper, typeof(LuaScriptingWrapper).GetMethod("GetTable", 0, new[] { typeof(string) }));

            return wrapper;
        }

        public static IScriptingWrapper InitializePowerShell(bool redirectStreams = false, bool loadSystemInstalledModules = false)
        {
            TelemetryDebugWriter.IsTracingDisabled = true;
            InitialSessionState initial = InitialSessionState.CreateDefault();
            if (loadSystemInstalledModules)
            {
                IEnumerable<string> modulesPath;
                using (Runspace tmp = RunspaceFactory.CreateRunspace(initial))
                {
                    modulesPath = TryDetectModulePath(tmp);
                }
                foreach (var elem in modulesPath)
                {
                    initial.ImportPSModulesFromPath(elem);
                }
            }
            Runspace runspace = RunspaceFactory.CreateRunspace(initial);
            runspace.Open();
            PowerShell ps = PowerShell.Create();
            ps.Runspace = runspace;
            PowerShellScriptingWrapper wrapper = new PowerShellScriptingWrapper(runspace, ps)
            {
                RedirectStreams = redirectStreams
            };
            
            wrapper.RegisterClassType(typeof(Logging));
            
            return wrapper;
        }

        private static IEnumerable<string> TryDetectModulePath(Runspace runspace)
        {
            try
            {
                List<string> modulePaths = new List<string>();
                string homeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                if (OperatingSystem.IsLinux())
                {
                    var version = runspace.Version;
                    for (int build = version.Build; build >= 0; build--)
                    {
                        string versionDir = new Version(version.Major, version.Minor, build).ToString();
                        string pwshStore = $"{homeDir}/.dotnet/tools/.store/powershell/{versionDir}/powershell/{versionDir}/tools";
                        if (!Directory.Exists(pwshStore))
                            continue;
                        var modules = Directory
                            .EnumerateDirectories(pwshStore, "Modules", new EnumerationOptions { RecurseSubdirectories = true })
                            .FirstOrDefault(x => x.Contains("/unix/"));
                        if (modules == null)
                            continue;
                        modulePaths.Add(modules);
                        break;
                    }
                    string pwshStoreFallback = $"/usr/local/share/powershell/Modules";
                    if (Directory.Exists(pwshStoreFallback))
                        modulePaths.Add(pwshStoreFallback);
                    pwshStoreFallback = $"/opt/microsoft/powershell/{version.Major}/Modules";
                    if (Directory.Exists(pwshStoreFallback))
                        modulePaths.Add(pwshStoreFallback);
                }
                if (OperatingSystem.IsWindows())
                {
                    string winDir = Environment.GetFolderPath(Environment.SpecialFolder.Windows);
                    string programsDir = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                    string pwshStore = $"{winDir}/System32/WindowsPowerShell/v1.0/Modules";
                    if (Directory.Exists(pwshStore))
                        modulePaths.Add(pwshStore);
                    pwshStore = $"{homeDir}/Documents/WindowsPowerShell/Modules";
                    if (Directory.Exists(pwshStore))
                        modulePaths.Add(pwshStore);
                    pwshStore = $"{programsDir}/WindowsPowerShell/Modules";
                    if (Directory.Exists(pwshStore))
                        modulePaths.Add(pwshStore);
                }
                return modulePaths;
            }
            catch (Exception)
            {
                return new string[0];
            }
        }

        private static IEnumerable<object> castEnumerable(IEnumerable col)
        {
            return col.Cast<object>();
        }

        private static IDictionary<object, object> castDictionary(IEnumerable dict)
        {
            return dict.Cast<DictionaryEntry>().ToDictionary(x => x.Key, x => x.Value);
        }

        public static IScriptingWrapper InitializePython(bool redirectStreams = false, Encoding encoding = null)
        {
            if (OperatingSystem.IsLinux())
                Runtime.PythonDLL = "libpython3.10.so";
            if (OperatingSystem.IsWindows())
                Runtime.PythonDLL = "python310.dll";
            PythonEngine.Initialize();
            PythonEngine.BeginAllowThreads();
            var gil = Py.GIL();
            var scope = Py.CreateScope();
            PythonScriptingWrapper wrapper = new PythonScriptingWrapper(gil, scope)
            {
                RedirectStreams = redirectStreams
            };
            
            wrapper.RegisterClassType(typeof(Logging));
            
            return wrapper;
        }
    }
}