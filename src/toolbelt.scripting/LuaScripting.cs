using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NLua;
using NLua.Exceptions;

namespace toolbelt
{
    internal class LuaScriptingWrapper : IScriptingWrapper, IDisposable
    {
        Lua lua;
        Exception lastError;

        internal LuaScriptingWrapper(Lua lua)
        {
            this.lua = lua;
            this.lastError = null;
        }

        ~LuaScriptingWrapper()
        {
            Dispose();
        }

        public void Dispose()
        {
            lua?.Dispose();
            lua = null;
            lastError = null;
        }

        private bool _redirectStreams;
        public bool RedirectStreams
        {
            get => _redirectStreams;
            set
            {
                _redirectStreams = value;
                if (value)
                {
                    if (GetVariable<object>(nameof(__print)) == null)
                        RegisterMethod(nameof(__print), this, typeof(LuaScriptingWrapper).GetMethod(nameof(__print), BindingFlags.Instance | BindingFlags.NonPublic));
                    ExecuteString($"__{nameof(__print)} = print; print = function(...) {nameof(__print)}({{...}}); end", "print_hook");
                }
                else
                {
                    ExecuteString($"if __{nameof(__print)} then print = __{nameof(__print)} end", "print_unhook");
                }
            }
        }

        public event Action<string> OutputDataReceived;
        public event Action<ScriptingErrorType, string> ErrorDataReceived;

        private void __print(LuaTable table)
        {
            var b = new StringBuilder();
            foreach (var v in table.Values)
            {
                if (b.Length > 0)
                {
                    b.Append(' ');
                }
                b.Append(v.ToString());
            }
            var text = b.ToString();
            OutputDataReceived?.Invoke(text);
        }

        private void SetAndWriteError(Exception ex)
        {
            if (ex == null)
                return;
            lastError = ex;
            if (RedirectStreams)
            {
                if (ex is LuaScriptException)
                    ErrorDataReceived?.Invoke(ScriptingErrorType.ScriptError, ex.Message);
                else if (ex is LuaException)
                    ErrorDataReceived?.Invoke(ScriptingErrorType.EngineError, ex.Message);
                else
                    ErrorDataReceived?.Invoke(ScriptingErrorType.Error, ex.ToString());
            }
            else
            {
                Console.Error.WriteLine(ex);
            }
        }

        public bool SetVariable<T>(string path, T value)
        {
            try
            {
                lua[path] = value;
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetVariable(string path, object value)
        {
            try
            {
                lua[path] = value;
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public T GetVariable<T>(string path)
        {
            try
            {
                object val = lua[path];
                if (val != null && typeof(T).IsAssignableFrom(val.GetType()))
                    return (T)val;
                return default(T);
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return default(T);
            }
        }

        public object GetVariable(string path)
        {
            try
            {
                object val = lua[path];
                return val;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool NewTable(string path)
        {
            try
            {
                lua[path] = null;
                lua.NewTable(path);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<T>(string path, IEnumerable<T> arr)
        {
            try
            {
                if (lua[path] == null)
                    lua.NewTable(path);
                LuaTable tbl = lua[path] as LuaTable;
                int i = 1;
                if (arr != null)
                {
                    foreach (var elem in arr)
                    {
                        tbl[i] = elem;
                        i++;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IEnumerable<object> arr)
        {
            try
            {
                if (lua[path] == null)
                    lua.NewTable(path);
                LuaTable tbl = lua[path] as LuaTable;
                int i = 1;
                if (arr != null)
                {
                    foreach (var elem in arr)
                    {
                        tbl[i] = elem;
                        i++;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable<K, V>(string path, IDictionary<K, V> dict)
        {
            try
            {
                if (lua[path] == null)
                    lua.NewTable(path);
                LuaTable tbl = lua[path] as LuaTable;
                if (dict != null)
                {
                    foreach (var elem in dict)
                    {
                        tbl[elem.Key] = elem.Value;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool SetTable(string path, IDictionary<object, object> dict)
        {
            try
            {
                if (lua[path] == null)
                    lua.NewTable(path);
                LuaTable tbl = lua[path] as LuaTable;
                if (dict != null)
                {
                    foreach (var elem in dict)
                    {
                        tbl[elem.Key] = elem.Value;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public IDictionary<K, V> GetTable<K, V>(string path)
        {
            try
            {
                LuaTable tbl = lua[path] as LuaTable;
                if (tbl != null)
                {
                    Dictionary<object, object> tmp = lua.GetTableDict(tbl);
                    Dictionary<K, V> res = new Dictionary<K, V>();
                    foreach (var item in tmp)
                    {
                        K key = (K)(object)item.Key;
                        V value = (V)(object)item.Value;
                        res[key] = value;
                    }
                    return res;
                }
                return null;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public IDictionary<object, object> GetTable(string path)
        {
            try
            {
                LuaTable tbl = lua[path] as LuaTable;
                if (tbl != null)
                {
                    Dictionary<object, object> res = lua.GetTableDict(tbl);
                    return res;
                }
                return null;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return null;
            }
        }

        public bool RegisterClassType(Type netType)
        {
            try
            {
                lua.DoString($"if not {netType.Name} then luanet.load_assembly('{netType.Assembly.FullName}','{netType.FullName}'); {netType.Name}=luanet.import_type('{netType.FullName}') end");
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool RegisterStaticMethod(string path, MethodInfo method) => RegisterMethod(path, null, method);

        public bool RegisterMethod(string path, object target, MethodInfo method)
        {
            try
            {
                lua.RegisterFunction(path, target, method);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool RegisterDelegateType(Type netType, Type engineType)
        {
            try
            {
                lua.RegisterLuaDelegateType(netType, engineType);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool ExecuteFile(string fileName)
        {
            try
            {
                lua.DoFile(fileName);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public bool ExecuteString(string chunk, string chunkName = "chunk")
        {
            try
            {
                lua.DoString(chunk, chunkName);
                return true;
            }
            catch (Exception ex)
            {
                SetAndWriteError(ex);
                return false;
            }
        }

        public Exception GetLastError()
        {
            return lastError;
        }
    }
}