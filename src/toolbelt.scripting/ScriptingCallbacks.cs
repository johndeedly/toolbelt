using NLua.Method;

namespace toolbelt
{
    public class NoInThreeOutDelegate<TRet1, TRet2, TRet3> : LuaDelegate
    {
        TRet1 CallFunction(ref TRet2 obj1, ref TRet3 obj2)
        {
            object[] args = { obj1, obj2 };
            object[] inArgs = { };
            int[] outArgs = { 0, 1 };
            object ret = base.CallFunction(args, inArgs, outArgs);
            obj1 = (TRet2)args[0];
            obj2 = (TRet3)args[1];
            return (TRet1)ret;
        }
    }

    public class TwoInTwoOutDelegate<T1, T2, TRet1, TRet2> : LuaDelegate
    {
        TRet1 CallFunction(T1 obj1, T2 obj2, ref TRet2 obj3)
        {
            object[] args = { obj1, obj2, obj3 };
            object[] inArgs = { obj1, obj2 };
            int[] outArgs = { 2 };
            object ret = base.CallFunction(args, inArgs, outArgs);
            obj3 = (TRet2)args[2];
            return (TRet1)ret;
        }
    }

    public class OneInTwoOutDelegate<T, TRet1, TRet2> : LuaDelegate
    {
        TRet1 CallFunction(T obj1, ref TRet2 obj2)
        {
            object[] args = { obj1, obj2 };
            object[] inArgs = { obj1 };
            int[] outArgs = { 1 };
            object ret = base.CallFunction(args, inArgs, outArgs);
            obj2 = (TRet2)args[1];
            return (TRet1)ret;
        }
    }

    public class NoInTwoOutDelegate<TRet1, TRet2> : LuaDelegate
    {
        TRet1 CallFunction(ref TRet2 obj)
        {
            object[] args = { obj };
            object[] inArgs = { };
            int[] outArgs = { 0 };
            object ret = base.CallFunction(args, inArgs, outArgs);
            obj = (TRet2)args[0];
            return (TRet1)ret;
        }
    }

    public class TwoInNoOutDelegate<T1, T2> : LuaDelegate
    {
        void CallFunction(T1 obj1, T2 obj2)
        {
            object[] args = new object[] { obj1, obj2 };
            object[] inArgs = new object[] { obj1, obj2 };
            int[] outArgs = new int[] { };
            base.CallFunction(args, inArgs, outArgs);
        }
    }

    public class OneInOneOutDelegate<T, TRet> : LuaDelegate
    {
        TRet CallFunction(T obj)
        {
            object[] args = { obj };
            object[] inArgs = { obj };
            int[] outArgs = { };
            object ret = base.CallFunction(args, inArgs, outArgs);
            return (TRet)ret;
        }
    }

    public class NoInOneOutDelegate<TRet> : LuaDelegate
    {
        TRet CallFunction()
        {
            object[] args = { };
            object[] inArgs = { };
            int[] outArgs = { };
            object ret = base.CallFunction(args, inArgs, outArgs);
            return (TRet)ret;
        }
    }

    public class OneInNoOutDelegate<T> : LuaDelegate
    {
        void CallFunction(T obj)
        {
            object[] args = new object[] { obj };
            object[] inArgs = new object[] { obj };
            int[] outArgs = new int[] { };
            base.CallFunction(args, inArgs, outArgs);
        }
    }

    public class NoInNoOutDelegate : LuaDelegate
    {
        void CallFunction()
        {
            object[] args = new object[] { };
            object[] inArgs = new object[] { };
            int[] outArgs = new int[] { };
            base.CallFunction(args, inArgs, outArgs);
        }
    }
}