using System;
using System.Threading.Tasks;

namespace toolbelt
{
    public static class DelegatesOnThreadsExtensions
    {
        public static Task OnThread(this Action action)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            Task task = ThreadPerTaskScheduler.Factory.StartNew(action);
            return task;
        }

        public static Task OnThread<T>(this Action<T> action, T arg)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            Task task = ThreadPerTaskScheduler.Factory.StartNew(() => action(arg));
            return task;
        }

        public static Task<T> OnThread<T>(this Func<T> func)
        {
            if (func == null)
                throw new ArgumentNullException(nameof(func));
            Task<T> task = ThreadPerTaskScheduler.Factory.StartNew(func);
            return task;
        }

        public static Task<TRes> OnThread<T, TRes>(this Func<T, TRes> func, T arg)
        {
            if (func == null)
                throw new ArgumentNullException(nameof(func));
            Task<TRes> task = ThreadPerTaskScheduler.Factory.StartNew(() => func(arg));
            return task;
        }
    }
}