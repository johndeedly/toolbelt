using System;
using System.Collections.Generic;
using System.Threading;

namespace toolbelt
{
    public static class AsyncJobs
    {

        public static void ExecuteInParallel<TElem>(IList<IList<TElem>> partitions, Action<TElem, int> job, Action<Exception> onError = null)
        {
            Thread[] worker = new Thread[partitions.Count];
            
            bool abort = false;
            for (int i = 0; i < partitions.Count; i++)
            {
                worker[i] = new Thread(new ParameterizedThreadStart((idx) =>
                {
                    foreach (TElem elem in partitions[(int)idx])
                    {
                        if (abort)
                            break;
                        try
                        {
                            job(elem, (int)idx);
                        }
                        catch (Exception ex)
                        {
                            abort = true;
                            if (onError != null)
                                onError(ex);
                            else
                                throw;
                            break;
                        }
                    }
                }));
                worker[i].Start(i);
            }

            for (int i = 0; i < partitions.Count; i++)
                worker[i].Join();
        }

        public static void ExecuteInParallel<TElem, TObj>(IList<IList<TElem>> partitions, Func<int, TObj> preparePartition, Action<TElem, int, TObj> job, Action<int, TObj> cleanupPartition, Action<Exception> onError = null)
        {
            Thread[] worker = new Thread[partitions.Count];
            TObj[] objects = new TObj[partitions.Count];
            for (int i = 0; i < partitions.Count; i++)
                objects[i] = preparePartition(i);

            bool abort = false;
            for (int i = 0; i < partitions.Count; i++)
            {
                worker[i] = new Thread(new ParameterizedThreadStart((idx) =>
                {
                    TObj obj = objects[(int)idx];
                    foreach (TElem elem in partitions[(int)idx])
                    {
                        if (abort)
                            break;
                        try
                        {
                            job(elem, (int)idx, obj);
                        }
                        catch (Exception ex)
                        {
                            abort = true;
                            cleanupPartition((int)idx, obj);
                            if (onError != null)
                                onError(ex);
                            else
                                throw;
                            break;
                        }
                    }
                }));
                worker[i].Start(i);
            }

            for (int i = 0; i < partitions.Count; i++)
                worker[i].Join();
            for (int i = 0; i < partitions.Count; i++)
                cleanupPartition(i, objects[i]);
        }
    }
}