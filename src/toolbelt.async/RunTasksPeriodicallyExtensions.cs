using System;
using System.Threading;
using System.Threading.Tasks;
using Cronos;

namespace toolbelt
{
    public static class RunTasksPeriodicallyExtensions
    {
        public static Task RunOnceAt(this Action action, string cronExpression = "* * * * *", bool includeSeconds = false)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            CronFormat format = includeSeconds ? CronFormat.IncludeSeconds : CronFormat.Standard;
            CronExpression expression = CronExpression.Parse(cronExpression, format);
            Task task = ThreadPerTaskScheduler.Factory.StartNew(() =>
            {
                DateTimeOffset? next = expression.GetNextOccurrence(DateTimeOffset.Now, TimeZoneInfo.Local);
                if (next != null)
                {
                    Thread.Sleep(next.Value.Subtract(DateTimeOffset.Now));
                    action.Invoke();
                }
            });
            return task;
        }

        public static Task RunPeriodicAt(this Action action, string cronExpression = "* * * * *", bool includeSeconds = false)
        {
            if (action == null)
                throw new ArgumentNullException(nameof(action));
            CronFormat format = includeSeconds ? CronFormat.IncludeSeconds : CronFormat.Standard;
            CronExpression expression = CronExpression.Parse(cronExpression, format);
            Task task = ThreadPerTaskScheduler.Factory.StartNew(() =>
            {
                DateTimeOffset? next = expression.GetNextOccurrence(DateTimeOffset.Now, TimeZoneInfo.Local);
                if (next != null)
                {
                    while (true)
                    {
                        Thread.Sleep(next.Value.Subtract(DateTimeOffset.Now));
                        action.Invoke();
                        next = expression.GetNextOccurrence(next.Value, TimeZoneInfo.Local);
                        if (next == null)
                            break;
                    }
                }
            });
            return task;
        }
    }
}