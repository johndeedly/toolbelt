using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace toolbelt
{
    public class ThreadPerTaskScheduler : TaskScheduler
    {
        public static TaskFactory Factory => taskFactory;

        private static readonly TaskFactory taskFactory = new TaskFactory(
            CancellationToken.None, TaskCreationOptions.DenyChildAttach,
            TaskContinuationOptions.None, new ThreadPerTaskScheduler());

        protected override IEnumerable<Task> GetScheduledTasks() => new Task[0];

        protected override void QueueTask(Task task)
        {
            new Thread(() =>
            {
                Logging.Debug("Task {@TaskId} executed on thread {@ThreadId}", task.Id, Thread.CurrentThread.ManagedThreadId);
                TryExecuteTask(task);
            })
            {
                IsBackground = true
            }.Start();
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            Logging.Debug("Task {@TaskId} prevented from being inlined", task.Id);
            return false;
        }

        private static readonly FieldInfo m_taskScheduler = typeof(Task).GetField("m_taskScheduler", BindingFlags.Instance | BindingFlags.NonPublic);
        
        public static Task SwitchToScheduler(Task task)
        {
            Task exec = task;
            TaskScheduler ts = (TaskScheduler)m_taskScheduler.GetValue(task);
            if (ts is not ThreadPerTaskScheduler)
            {
                exec = ThreadPerTaskScheduler.Factory.StartNew(() => task).Unwrap();
            }
            return exec;
        }

        public static Task<T> SwitchToScheduler<T>(Task<T> task)
        {
            Task<T> exec = task;
            TaskScheduler ts = (TaskScheduler)m_taskScheduler.GetValue(task);
            if (ts is not ThreadPerTaskScheduler)
            {
                exec = ThreadPerTaskScheduler.Factory.StartNew(() => task).Unwrap();
            }
            return exec;
        }
    }
}