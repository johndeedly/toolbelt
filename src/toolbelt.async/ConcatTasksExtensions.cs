using System;
using System.Threading;
using System.Threading.Tasks;
using Cronos;

namespace toolbelt
{
    //
    // The following use cases are supported:
    // 1. Task without return value, delegate without return value follows
    // 2. Task without return value, delegate with return value follows
    // 3. Task without return value, task without return value follows
    // 4. Task without return value, task with return value follows
    // 5. Task with return value, delegate takes param, no new return value
    // 6. Task with return value, delegate takes param, with new return value
    // 7. Task with return value, delegate leaves param, param of task is returned
    // 8. Task with return value, task leaves param, param of task is returned
    //
    public static class ConcatTasksExtensions
    {
        //
        // 1. Task without return value, delegate without return value follows
        //
        public static Task Concat(this Task task, Action continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    continuation.Invoke();
                }
            });
        }

        //
        // 2. Task without return value, delegate with return value follows
        //
        public static Task<T> Concat<T>(this Task task, Func<T> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    return continuation.Invoke();
                }
            });
        }

        //
        // 3. Task without return value, task without return value follows
        //
        public static Task Concat(this Task task, Func<Task> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    return ThreadPerTaskScheduler.SwitchToScheduler(continuation.Invoke());
                }
            }).Unwrap();
        }

        //
        // 4. Task without return value, task with return value follows
        //
        public static Task<T> Concat<T>(this Task task, Func<Task<T>> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    return ThreadPerTaskScheduler.SwitchToScheduler<T>(continuation.Invoke());
                }
            }).Unwrap();
        }

        //
        // 5. Task with return value, delegate takes param, no new return value
        //
        public static Task Concat<T>(this Task<T> task, Action<T> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler<T>(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    continuation.Invoke(t.Result);
                }
            });
        }

        //
        // 6. Task with return value, delegate takes param, with new return value
        //
        public static Task<TRes> Concat<T, TRes>(this Task<T> task, Func<T, TRes> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler<T>(task);
            return next.ContinueWith(t => 
            {
                using (t)
                {
                    return continuation.Invoke(t.Result);
                }
            });
        }

        //
        // 7. Task with return value, delegate leaves param, param of task is returned
        //
        public static Task<T> Concat<T>(this Task<T> task, Action continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler<T>(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    continuation.Invoke();
                    return t.Result;
                }
            });
        }

        //
        // 8. Task with return value, task leaves param, param of task is returned
        //
        public static Task<T> Concat<T>(this Task<T> task, Func<Task> continuation)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            if (continuation == null)
                throw new ArgumentNullException(nameof(continuation));
            Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            return next.ContinueWith(t =>
            {
                using (t)
                {
                    return ThreadPerTaskScheduler.SwitchToScheduler(continuation.Invoke()).Concat(() => t.Result);
                }
            }).Unwrap();
        }
    }
}