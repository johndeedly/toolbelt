using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NStack;
using Terminal.Gui;

namespace toolbelt
{
    public class ShellInfo
    {
        public string FileName { get; set; } = null;
        public string Arguments { get; set; } = null;
        public string WorkingDirectory { get; set; } = null;
        public int? Id { get; set; } = null;
        public int? ExitCode { get; set; } = null;
    }

    public static class ShellUtils
    {
        public static Task RunShellAsync(string fileName, string arguments = null, string workingDirectory = null, ShellInfo info = null, Stream outputStream = null, Stream errorStream = null, Stream inputStream = null)
        {
            return ThreadPerTaskScheduler.Factory.StartNew(delegate
            {
                Process proc = new Process();
                UTF8Encoding enc = new UTF8Encoding(false);
                proc.StartInfo = new ProcessStartInfo
                {
                    FileName = fileName,
                    Arguments = arguments ?? string.Empty
                };
                if (info != null)
                {
                    info.FileName = fileName;
                    info.Arguments = arguments;
                }
                if (workingDirectory != null && Directory.Exists(workingDirectory))
                {
                    proc.StartInfo.WorkingDirectory = workingDirectory;
                    if (info != null)
                        info.WorkingDirectory = workingDirectory;
                }
                outputStream = outputStream ?? Console.OpenStandardOutput();
                if (outputStream.CanWrite)
                {
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.StandardOutputEncoding = enc;
                }
                errorStream = errorStream ?? Console.OpenStandardError();
                if (outputStream.CanWrite)
                {
                    proc.StartInfo.RedirectStandardError = true;
                    proc.StartInfo.StandardErrorEncoding = enc;
                }
                inputStream = inputStream ?? Console.OpenStandardInput();
                if (inputStream.CanRead && inputStream.CanSeek && inputStream.Length > 0)
                {
                    proc.StartInfo.RedirectStandardInput = true;
                }
                proc.Start();
                if (info != null)
                    info.Id = proc.Id;
                Task stdout = null, stderr = null, stdin = null;
                if (outputStream.CanWrite)
                    stdout = proc.StandardOutput.BaseStream.CopyToAsync(outputStream);
                if (errorStream.CanWrite)
                    stderr = proc.StandardError.BaseStream.CopyToAsync(errorStream);
                if (inputStream.CanRead && inputStream.CanSeek && inputStream.Length > 0)
                {
                    stdin = ThreadPerTaskScheduler.Factory.StartNew(delegate
                    {
                        inputStream.CopyTo(proc.StandardInput.BaseStream);
                        proc.StandardInput.Close();
                    });
                }
                stdin?.Await();
                proc.WaitForExit();
                if (info != null)
                    info.ExitCode = proc.ExitCode;
                stdout?.Await();
                stderr?.Await();
            });
        }

        public static Task<string> RunShellTextAsync(string fileName, string arguments = null, string workingDirectory = null, ShellInfo info = null, Stream errorStream = null, Stream inputStream = null)
        {
            return ThreadPerTaskScheduler.Factory.StartNew(delegate
            {
                using (MemoryStream mem = new MemoryStream())
                {
                    RunShellAsync(fileName, arguments, workingDirectory, info, mem, errorStream, inputStream).Await();
                    string txt = Encoding.UTF8.GetString(mem.ToArray());
                    return txt;
                }
            });
        }

        private static string GetArgumentValue(string txt)
        {
            if (txt == null)
                return string.Empty;
            if (txt.Contains("\"") || txt.Contains("\\") || txt.Contains(" "))
                return string.Concat("\"", txt.Replace("\\", "\\\\").Replace("\"", "\\\""), "\"");
            return txt;
        }

        private static void GetRuntimeArguments(out string parentProcessId, out string runtimeConfig, out string depsFile)
        {
            string[] split;
            // Environment.GetCommandLineArgs doesn't include arguments passed to the runtime.
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                split = File.ReadAllText($"/proc/{Process.GetCurrentProcess().Id}/cmdline")
                    .Split(new[] { '\0' })
                    .Select(GetArgumentValue)
                    .ToArray();
            else
                throw new PlatformNotSupportedException($"{nameof(GetRuntimeArguments)} is unsupported on this platform");

            parentProcessId = null;
            runtimeConfig = null;
            depsFile = null;
            for (int i = 0; i < split.Length - 1; i++)
            {
                if (split[i] == "--parentprocessid")
                    parentProcessId = split[i + 1];
                if (split[i] == "--runtimeconfig")
                    runtimeConfig = split[i + 1];
                if (split[i] == "--depsfile")
                    depsFile = split[i + 1];
            }
        }

        public static Task RunChildAsync(string arguments = null, string workingDirectory = null, ShellInfo info = null, Stream outputStream = null, Stream errorStream = null, Stream inputStream = null)
        {
            string hostFilename = Process.GetCurrentProcess().MainModule.FileName;
            string environmentArguments = string.Join(" ", Environment.GetCommandLineArgs().Skip(1).Select(GetArgumentValue));
            string hostArguments = arguments ?? environmentArguments;
            string parentProcessId, runtimeConfig, depsFile;
            GetRuntimeArguments(out parentProcessId, out runtimeConfig, out depsFile);

            // application is running as 'testhost'
            // try to find parent 'dotnet' host process.
            if (hostFilename.EndsWith("/testhost"))
            {
                if (parentProcessId == null)
                    throw new NotSupportedException("/testhost process without argument for parent dotnet process");
                int parentProcessIdVal = int.Parse(parentProcessId);
                hostFilename = Process.GetProcessById(parentProcessIdVal)?.MainModule.FileName;
                if (hostFilename == null || !hostFilename.EndsWith("/dotnet"))
                    throw new NotSupportedException("/testhost's dotnet process not found");
            }

            // application is running as 'dotnet'.
            if (hostFilename.EndsWith("/dotnet"))
            {
                string entryAssembly = new Uri(Assembly.GetEntryAssembly().Location).LocalPath;
                if (runtimeConfig == null)
                    runtimeConfig = $"{Path.GetDirectoryName(entryAssembly)}/{Path.GetFileNameWithoutExtension(entryAssembly)}.runtimeconfig.json";
                if (depsFile == null)
                    depsFile = $"{Path.GetDirectoryName(entryAssembly)}/{Path.GetFileNameWithoutExtension(entryAssembly)}.deps.json";
                hostArguments = $"exec --runtimeconfig \"{runtimeConfig}\" --depsfile \"{depsFile}\" \"{entryAssembly}\" {hostArguments}";
            }

            return RunShellAsync(hostFilename, hostArguments, workingDirectory, info, outputStream, errorStream, inputStream);
        }

        public static int PopupWindow(string title, string question, params ustring[] buttons)
        {
            int res = openPopup(title, question, buttons, false);
            return res;
        }

        public static int PopupErrorWindow(string title, string question, params ustring[] buttons)
        {
            int res = openPopup(title, question, buttons, true);
            return res;
        }

        private static int openPopup(string title, string question, ustring[] buttons, bool error)
        {
            bool wasme = false;
            int res = -1;
            try
            {
                if (Application.Top == null)
                {
                    wasme = true;
                    Application.UseSystemConsole = true;
                    Application.Init();
                }

                Toplevel top = new Toplevel()
                {
                    X = 0,
                    Y = 0,
                    Width = Dim.Fill(),
                    Height = Dim.Fill()
                };
                top.Resized += (e) =>
                {
                    Application.Refresh();
                };
                Window win = new Window(title)
                {
                    X = Pos.Center(),
                    Y = Pos.Center(),
                    Width = Dim.Percent(75),
                    Height = Dim.Percent(75)
                };
                if (error)
                    win.ColorScheme = Colors.Error;
                Label text = new Label()
                {
                    X = 1,
                    Y = 1,
                    Width = Dim.Fill(1),
                    Height = Dim.Fill(2),
                    Text = question
                };
                if (error)
                    text.ColorScheme = Colors.Error;
                win.Add(text);
                Button[] btns = new Button[buttons.Length];
                for (int i = 0; i < buttons.Length; i++)
                {
                    btns[i] = new Button(buttons[i])
                    {
                        X = Pos.Percent((i + 1) * 95 / (buttons.Length + 1)),
                        Y = Pos.AnchorEnd(1),
                        Height = 1
                    };
                    int id = i;
                    btns[i].Clicked += () => { res = id; Application.RequestStop(); };
                    if (error)
                        btns[i].ColorScheme = Colors.Error;
                    win.Add(btns[i]);
                }
                top.Add(win);

                Application.Run(top);
                return res;
            }
            finally
            {
                if (wasme)
                    Application.Shutdown();
            }
        }
    }
}