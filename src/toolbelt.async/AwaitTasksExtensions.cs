using System;
using System.Threading.Tasks;

namespace toolbelt
{
    public static class AwaitTasksExtensions
    {
        public static void Await(this Task task)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            using (task)
            using (Task next = ThreadPerTaskScheduler.SwitchToScheduler(task))
            {
                next.ConfigureAwait(true).GetAwaiter().GetResult();
            }
        }

        public static void Await(this Task task, TimeSpan timeout, Action onTimeout)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            Task next = ThreadPerTaskScheduler.SwitchToScheduler(task);
            Task delay = ThreadPerTaskScheduler.SwitchToScheduler(Task.Delay(timeout)).ContinueWith(t => t.Dispose());
            ThreadPerTaskScheduler.Factory.ContinueWhenAny(new [] { next, delay }, t =>
            {
                if (t == delay)
                {
                    if (onTimeout == null)
                        throw new TimeoutException();
                    onTimeout.Invoke();
                    return;
                }
                next?.Dispose();
                task?.Dispose();
            }).ContinueWith(t => t.Dispose()).ConfigureAwait(true).GetAwaiter().GetResult();
        }

        public static T Await<T>(this Task<T> task)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            using (task)
            using (Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler<T>(task))
            {
                return next.ConfigureAwait(true).GetAwaiter().GetResult();
            }
        }

        public static T Await<T>(this Task<T> task, TimeSpan timeout, Action onTimeout)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));
            Task<T> next = ThreadPerTaskScheduler.SwitchToScheduler<T>(task);
            Task delay = ThreadPerTaskScheduler.SwitchToScheduler(Task.Delay(timeout)).ContinueWith(t => t.Dispose());
            return ThreadPerTaskScheduler.Factory.ContinueWhenAny(new [] { next, delay }, t =>
            {
                if (t == delay)
                {
                    if (onTimeout == null)
                        throw new TimeoutException();
                    onTimeout.Invoke();
                    return default(T);
                }
                using (task)
                using (next)
                {
                    return next.Result;
                }
            }).ContinueWith(t =>
            {
                using (t)
                {
                    return t.Result;
                }
            }).ConfigureAwait(true).GetAwaiter().GetResult();
        }
    }
}