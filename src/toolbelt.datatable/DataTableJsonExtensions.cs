﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace toolbelt
{
    public static class DataTableJsonExtensions
    {
        class Ref<T>
        {
            public T Value;

            public static Ref<T> Create(T val)
            {
                Ref<T> elem = new Ref<T>();
                elem.Value = val;
                return elem;
            }
        }

        public static DataTable FromJson(this DataTable dt, string json, string prefix = null)
        {
            using (JsonDocument jdoc = JsonDocument.Parse(json))
            {
                return FromJson(dt, jdoc.RootElement, prefix);
            }
        }

        public static DataTable FromJson(this DataTable dt, JsonElement elem, string prefix = null)
        {
            DataRow _dr = null;
            TraverseJsonAny(dt, ref _dr, elem, prefix);
            return dt;
        }

        static Regex arrayOfRows = new Regex(@"^\.\[\]\.", RegexOptions.Compiled | RegexOptions.Singleline);

        public static DataTable ToJson(this DataTable dt, Stream s)
        {
            using (var jsonWriter = new Utf8JsonWriter(s))
            {
                jsonWriter.WriteStartArray();

                foreach (DataRow dr in dt.Rows)
                {
                    jsonWriter.WriteStartObject();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        string columnName = dc.ColumnName;
                        columnName = arrayOfRows.Replace(columnName, string.Empty);

                        object val = dr[dc];
                        if (val != DBNull.Value && val != null)
                        {
                            if (val is string)
                            {
                                jsonWriter.WriteString(columnName, (string)val);
                            }
                            if (val is bool)
                            {
                                jsonWriter.WriteBoolean(columnName, (bool)val);
                            }
                            if (val.IsNumber())
                            {
                                jsonWriter.WriteNumber(columnName, (decimal)val);
                            }
                            if (val is byte[])
                            {
                                jsonWriter.WriteBase64String(columnName, (byte[])val);
                            }
                        }
                    }
                    jsonWriter.WriteEndObject();
                }
                jsonWriter.WriteEndArray();

                jsonWriter.Flush();
                s.WriteByte(10);
            }

            return dt;
        }
        
        public static DataTable ToJson(this DataTable dt, out JsonDocument json)
        {
            using (var mem = new MemoryStream())
            {
                ToJson(dt, mem);
                mem.Seek(0, SeekOrigin.Begin);
                byte[] data = new byte[mem.Length];
                mem.Read(data, 0, data.Length);
                json = JsonDocument.Parse(Encoding.UTF8.GetString(data));
            }

            return dt;
        }

        private static void TraverseJsonAny(DataTable dt, ref DataRow dr, JsonElement elem, string prefix)
        {
            if (elem.ValueKind == JsonValueKind.Array)
            {
                List<Ref<DataRow>> drs = new List<Ref<DataRow>>();
                drs.Add(Ref<DataRow>.Create(dr));
                // new row references will be created here
                TraverseJsonArray(dt, drs, elem, prefix);
            }
            else if (elem.ValueKind == JsonValueKind.Object)
            {
                // new row references will be created here if the object contains arrays
                TraverseJsonObject(dt, ref dr, elem, prefix);
            }
            else if (elem.ValueKind != JsonValueKind.Null && elem.ValueKind != JsonValueKind.Undefined)
            {
                // only here rows and colums will be created
                TraverseJsonValue(dt, ref dr, elem, prefix);
            }
        }

        private static void TraverseJsonArray(DataTable dt, List<Ref<DataRow>> drs, JsonElement elem, string prefix)
        {
            // it will be at least one element in drs with a reference to a null object in it
            var @enum = elem.EnumerateArray();
            if (@enum.Any())
            {
                if (prefix == null)
                    prefix = ".";
                string newPrefix = string.Concat(prefix, "[]");
                int i = 0;
                Ref<DataRow> newdr = null;
                foreach (var item in @enum)
                {
                    if (i == drs.Count && newdr.Value != null)
                    {
                        newdr = Ref<DataRow>.Create(null);
                    }
                    else
                    {
                        newdr = drs[i];
                    }
                    TraverseJsonAny(dt, ref newdr.Value, item, newPrefix);
                    if (i == drs.Count)
                    {
                        if (newdr.Value != null)
                        {
                            drs.Add(newdr);
                            i++;
                        }
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        private static void TraverseJsonObject(DataTable dt, ref DataRow dr, JsonElement elem, string prefix)
        {
            var @enum = elem.EnumerateObject();
            var @noarr = @enum.Where(x => x.Value.ValueKind != JsonValueKind.Array);
            if (@noarr.Any())
            {
                foreach (var prop in @noarr)
                {
                    string newPrefix = $"{prefix}.{prop.Name}";
                    TraverseJsonAny(dt, ref dr, prop.Value, newPrefix);
                }
            }
            var @array = @enum.Where(x => x.Value.ValueKind == JsonValueKind.Array);
            if (@array.Any())
            {
                List<Ref<DataRow>> drs = new List<Ref<DataRow>>();
                drs.Add(Ref<DataRow>.Create(dr));
                foreach (var prop in @array)
                {
                    string newPrefix = $"{prefix}.{prop.Name}";
                    TraverseJsonArray(dt, drs, prop.Value, newPrefix);
                }
            }
        }

        private static void TraverseJsonValue(DataTable dt, ref DataRow dr, JsonElement elem, string prefix)
        {
            // the value is not null or undefined at this point
            if (dr == null)
            {
                dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
            DataColumn dc = null;
            if (dt.Columns.Contains(prefix))
                dc = dt.Columns[prefix];
            if (elem.ValueKind == JsonValueKind.False || elem.ValueKind == JsonValueKind.True)
            {
                if (dc == null)
                    dc = dt.Columns.Add(prefix, typeof(bool));
                dr[dc] = elem.GetBoolean();
            }
            if (elem.ValueKind == JsonValueKind.Number)
            {
                if (dc == null)
                    dc = dt.Columns.Add(prefix, typeof(decimal));
                dr[dc] = elem.GetDecimal();
            }
            if (elem.ValueKind == JsonValueKind.String)
            {
                if (dc == null)
                    dc = dt.Columns.Add(prefix, typeof(string));
                dr[dc] = elem.GetString();
            }
        }
    }
}
