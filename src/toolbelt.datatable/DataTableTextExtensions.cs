using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace toolbelt
{
    public static class DataTableTextExtensions
    {
        public static DataTable ToText(this DataTable dt, Stream s)
        {
            if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                int[] colWidth = new int[dt.Columns.Count];
                for (int c = 0; c < dt.Columns.Count; c++)
                {
                    colWidth[c] = Math.Max(3, dt.Columns[c].ColumnName.Length);
                    for (int r = 0; r < dt.Rows.Count; r++)
                    {
                        if (dt.Rows[r][c] != DBNull.Value && dt.Rows[r][c] != null)
                        {
                            colWidth[c] = Math.Max(3, Math.Max(colWidth[c], dt.Rows[r][c].ToString().Length));
                        }
                    }
                }

                byte[] data = Encoding.UTF8.GetBytes(string.Join("  ", dt.Columns.OfType<DataColumn>().Zip(colWidth, (a, b) => new { First = a, Second = b }).Select(x => x.First.ColumnName.PadLeft(x.Second))));
                s.Write(data, 0, data.Length);
                s.WriteByte(10);

                data = Encoding.UTF8.GetBytes(string.Join("  ", colWidth.Select(x => new string('-', x))));
                s.Write(data, 0, data.Length);
                s.WriteByte(10);

                foreach (DataRow dr in dt.Rows)
                {
                    data = Encoding.UTF8.GetBytes(string.Join("  ", dt.Columns.OfType<DataColumn>().Zip(colWidth, (a, b) => new { First = a, Second = b }).Select(x => GetValue(dr, x.First).PadLeft(x.Second))));
                    s.Write(data, 0, data.Length);
                    s.WriteByte(10);
                }
            }
            return dt;
        }

        public static DataTable ToTextTransposed(this DataTable dt, Stream s)
        {
            if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                int nameWidth = Math.Max(3, dt.Columns.OfType<DataColumn>().Max(x => x.ColumnName.Length));
                int[] rowWidth = new int[dt.Rows.Count];
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    for (int c = 0; c < dt.Columns.Count; c++)
                    {
                        if (dt.Rows[r][c] != DBNull.Value && dt.Rows[r][c] != null)
                        {
                            rowWidth[r] = Math.Max(3, Math.Max(rowWidth[r], dt.Rows[r][c].ToString().Length));
                        }
                    }
                }

                foreach (DataColumn dc in dt.Columns)
                {
                    byte[] data = Encoding.UTF8.GetBytes($"{dc.ColumnName.PadLeft(nameWidth)}  ");
                    s.Write(data, 0, data.Length);
                    data = Encoding.UTF8.GetBytes(string.Join("  ", dt.Rows.OfType<DataRow>().Zip(rowWidth, (a, b) => new { First = a, Second = b }).Select(x => GetValue(x.First, dc).PadLeft(x.Second))));
                    s.Write(data, 0, data.Length);
                    s.WriteByte(10);
                }
            }
            return dt;
        }

        private static string tab = "\t";
        private static string escapedtab = "  ";
        private static string newline = "\n";
        private static string escapednewline = "-/-";

        private static string GetValue(DataRow dr, DataColumn dc)
        {
            string value = dr[dc].ToString();
            if (value.Any(x => x == tab[0]))
                value = value.Replace(tab, escapedtab);
            if (value.Any(x => x == newline[0]))
                value = value.Replace(newline, escapednewline);
            return value;
        }
    }
}
