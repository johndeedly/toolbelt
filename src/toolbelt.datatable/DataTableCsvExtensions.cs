﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;

namespace toolbelt
{
    public static class DataTableCsvExtensions
    {
        public static DataTable FromCsv(this DataTable dt, Stream s, bool headerLine = false)
        {
            CsvConfiguration conf = new CsvConfiguration(CultureInfo.InvariantCulture);
            conf.HasHeaderRecord = headerLine;
            using (var reader = new StreamReader(s, new UTF8Encoding(false)))
            using (var csv = new CsvReader(reader, conf))
            using (var dr = new CsvDataReader(csv))
            {
                dt.Load(dr);
            }
            return dt;
        }

        public static DataTable ToCsv(this DataTable dt, Stream s)
        {
            if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                byte[] data = Encoding.UTF8.GetBytes(string.Join(",", dt.Columns.OfType<DataColumn>().Select(x => GetValueString(x.ColumnName))));
                s.Write(data, 0, data.Length);
                s.WriteByte(10);

                foreach (DataRow dr in dt.Rows)
                {
                    data = Encoding.UTF8.GetBytes(string.Join(",", dt.Columns.OfType<DataColumn>().Select(x => GetValue(dr, x))));
                    s.Write(data, 0, data.Length);
                    s.WriteByte(10);
                }
            }
            return dt;
        }

        public static DataTable ToCsvTransposed(this DataTable dt, Stream s)
        {
            if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    byte[] data = Encoding.UTF8.GetBytes($"{GetValueString(dc.ColumnName)},");
                    s.Write(data, 0, data.Length);
                    data = Encoding.UTF8.GetBytes(string.Join(",", dt.Rows.OfType<DataRow>().Select(x => GetValue(x, dc))));
                    s.Write(data, 0, data.Length);
                    s.WriteByte(10);
                }
            }
            return dt;
        }

        private static string quote = "\"";
        private static string doublequote = "\"\"";

        private static string GetValue(DataRow dr, DataColumn dc)
        {
            return GetValueString(dr[dc]);
        }

        private static string GetValueString(object val)
        {
            if (val == null || val == DBNull.Value)
                return string.Empty;
            string value = val.ToString();
            // , including ; and \t because of MS Excel 
            if (value.Any(x => x == ',' || x == ';' || x == '\t' || x == '"' || x == '\n'))
                value = string.Concat(quote, value.Replace(quote, doublequote), quote);
            return value;
        }
    }
}
