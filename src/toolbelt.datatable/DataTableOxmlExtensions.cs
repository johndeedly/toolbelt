﻿using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace toolbelt
{
    public static class DataTableOxmlExtensions
    {
        public static DataTable ToOxml(this DataTable dt, Stream s)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument
                .Create(s, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart;
                SheetData sheetData;
                InitSpreadsheet(spreadsheetDocument, out workbookPart, out sheetData);

                if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
                {
                    Row headerRow = new Row();
                    sheetData.AppendChild(headerRow);

                    foreach (DataColumn col in dt.Columns)
                    {
                        Cell cell = new Cell();
                        cell.DataType = CellValues.String;
                        cell.CellValue = new CellValue(col.ColumnName);
                        headerRow.AppendChild(cell);
                    }

                    foreach (DataRow dr in dt.Rows)
                    {
                        Row dataRow = new Row();
                        sheetData.AppendChild(dataRow);
                        foreach (DataColumn dc in dt.Columns)
                        {
                            AddCell(dataRow, dr, dc);
                        }
                    }
                }

                workbookPart.Workbook.Save();
                spreadsheetDocument.Close();
            }

            return dt;
        }

        public static DataTable ToOxmlTransposed(this DataTable dt, Stream s)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.
                Create(s, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart;
                SheetData sheetData;
                InitSpreadsheet(spreadsheetDocument, out workbookPart, out sheetData);

                if (dt != null && dt.Rows.Count > 0 && dt.Columns.Count > 0)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        Row dataRow = new Row();
                        sheetData.AppendChild(dataRow);
                        
                        Cell headerCell = new Cell();
                        headerCell.DataType = CellValues.String;
                        headerCell.CellValue = new CellValue(dc.ColumnName);
                        dataRow.AppendChild(headerCell);
                        
                        foreach (DataRow dr in dt.Rows)
                        {
                            AddCell(dataRow, dr, dc);
                        }
                    }
                }

                workbookPart.Workbook.Save();
                spreadsheetDocument.Close();
            }

            return dt;
        }

        private static void AddCell(Row dataRow, DataRow dr, DataColumn dc)
        {
            Cell cell = new Cell();
            object obj = dr[dc];
            if (obj != null && obj != DBNull.Value)
            {
                if (dc.DataType.IsNumber())
                {
                    cell.DataType = CellValues.Number;
                    cell.CellValue = new CellValue(decimal.Parse(obj.ToString()));
                }
                else
                {
                    cell.DataType = CellValues.String;
                    string val = Regex.Replace(obj.ToString().Trim(), @"\p{C}+", string.Empty);
                    cell.CellValue = new CellValue(val);
                }
            }
            dataRow.AppendChild(cell);
        }

        private static void InitSpreadsheet(SpreadsheetDocument spreadsheetDocument, out WorkbookPart workbookPart, out SheetData sheetData)
        {
            workbookPart = spreadsheetDocument.AddWorkbookPart();
            Sheets sheets = new Sheets();
            workbookPart.Workbook = new Workbook(sheets);

            WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            sheetData = new SheetData();
            worksheetPart.Worksheet = new Worksheet(sheetData);

            Sheet sheet = new Sheet()
            {
                Id = workbookPart.GetIdOfPart(worksheetPart),
                SheetId = 1
            };
            sheets.Append(sheet);
        }
    }
}
