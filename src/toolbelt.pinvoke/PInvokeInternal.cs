using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace toolbelt
{
    static class PInvokeInternal
    {
        private static readonly Func<Type[], Type> MakeNewCustomDelegate = (Func<Type[], Type>)Delegate.CreateDelegate(typeof(Func<Type[], Type>), typeof(Expression).Assembly.GetType("System.Linq.Expressions.Compiler.DelegateHelpers").GetMethod("MakeNewCustomDelegate", BindingFlags.NonPublic | BindingFlags.Static));

        public static Type CreateCustomDelegateType(MethodInfo method)
        {
            // the last element after the method parameters is the return type
            Type[] parameters = method.GetParameters().Select(x => x.ParameterType).ToArray();
            Type returnType = method.ReturnType;
            return CreateCustomDelegateType(parameters, returnType);
        }

        public static Type CreateCustomDelegateType(Type[] parameters = null, Type returnType = null)
        {
            // set default values
            if (parameters == null)
                parameters = new Type[0];
            if (returnType == null)
                returnType = typeof(void);
            // the last element after the method parameters is the return type
            Type[] args = new Type[parameters.Length + 1];
            if (parameters != null)
                parameters.CopyTo(args, 0);
            args[args.Length - 1] = returnType;
            // hidden gem
            return MakeNewCustomDelegate(args);
        }
    }
}