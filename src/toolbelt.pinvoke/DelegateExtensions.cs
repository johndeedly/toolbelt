using System;
using System.Reflection;

namespace toolbelt
{
    public static class DelegateExtensions
    {
        public static D Cast<D>(this Delegate del) where D : Delegate
        {
            if (del is D delD)
                return delD;
            // create a delegate of type D to the invoke function of the base delegate, which then calls the method's target
            MethodInfo mi = del.GetType().GetMethod("Invoke");
            return Delegate.CreateDelegate(typeof(D), del, mi) as D;
        }
    }
}