using System;
using System.Runtime.InteropServices;

namespace toolbelt
{
    public class SafeNativeLibHandle : SafeHandle
    {
        public override bool IsInvalid => handle == IntPtr.Zero;
        
        public SafeNativeLibHandle(IntPtr handle) : base(IntPtr.Zero, true)
        {
            SetHandle(handle);
        }

        public IntPtr GetFunctionHandle(string funcName)
        {
            if (IsInvalid)
                return IntPtr.Zero;
            return NativeLibrary.GetExport(handle, funcName);
        }

        protected override bool ReleaseHandle()
        {
            if (!IsInvalid)
                NativeLibrary.Free(handle);
            return true;
        }
    }
}