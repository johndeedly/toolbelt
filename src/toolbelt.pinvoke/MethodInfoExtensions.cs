using System;
using System.Reflection;

namespace toolbelt
{
    public static class MethodInfoExtensions
    {
        public static D ToDelegate<D>(this MethodInfo method, object target = null) where D : Delegate
        {
            // casting directly to delegate type D does not work
            Delegate delFunc = ToDelegate(method, target);
            // instead, create a delegate of type D to the invoke function of the base delegate, which then calls the method
            return delFunc.Cast<D>();
        }
        
        public static Delegate ToDelegate(this MethodInfo method, object target = null)
        {
            Type delT = PInvokeInternal.CreateCustomDelegateType(method);
            // create a delegate with the newly constructed method delegate type
            return Delegate.CreateDelegate(delT, target, method);
        }
    }
}