using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;

namespace toolbelt
{
    public static class NativeUtils
    {
        private static readonly Func<Type[], Type> MakeNewCustomDelegate = (Func<Type[], Type>)Delegate.CreateDelegate(typeof(Func<Type[], Type>), typeof(Expression).Assembly.GetType("System.Linq.Expressions.Compiler.DelegateHelpers").GetMethod("MakeNewCustomDelegate", BindingFlags.NonPublic | BindingFlags.Static));
        private static readonly Dictionary<string, SafeNativeLibHandle> Natives = new Dictionary<string, SafeNativeLibHandle>();

        public static D GetDelegateToNativeFunction<D>(string libPath, string funcName) where D : Delegate
        {
            Delegate delFunc = GetDelegateToNativeFunction(libPath, funcName, typeof(D));
            return delFunc.Cast<D>();
        }

        public static Delegate GetDelegateToNativeFunction(string libPath, string funcName, Type delegateT)
        {
            SafeNativeLibHandle lib;
            if (!Natives.TryGetValue(libPath, out lib))
            {
                lib = new SafeNativeLibHandle(NativeLibrary.Load(libPath));
                Natives[libPath] = lib;
            }
            IntPtr func = lib.GetFunctionHandle(funcName);
            // ensures that generic types can be used, too
            MethodInfo mi = delegateT.GetMethod("Invoke");
            Type delT = PInvokeInternal.CreateCustomDelegateType(mi);
            return Marshal.GetDelegateForFunctionPointer(func, delT);
        }

        public static Delegate GetDelegateToNativeFunction(string libPath, string funcName, Type[] parameters = null, Type returnType = null)
        {
            // set default values
            if (parameters == null)
                parameters = new Type[0];
            if (returnType == null)
                returnType = typeof(void);
            
            SafeNativeLibHandle lib;
            if (!Natives.TryGetValue(libPath, out lib))
            {
                lib = new SafeNativeLibHandle(NativeLibrary.Load(libPath));
                Natives[libPath] = lib;
            }
            IntPtr func = lib.GetFunctionHandle(funcName);
            Type delT = PInvokeInternal.CreateCustomDelegateType(parameters, returnType);
            return Marshal.GetDelegateForFunctionPointer(func, delT);
        }
    }
}