using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Warning(string messageTemplate)
        {
            Log.Warning(messageTemplate);
        }

        public static void Warning<T>(string messageTemplate, T propertyValue)
        {
            Log.Warning<T>(messageTemplate, propertyValue);
        }

        public static void Warning<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Warning<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Warning<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Warning<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Warning(string messageTemplate, params object[] propertyValues)
        {
            Log.Warning(messageTemplate, propertyValues);
        }

        public static void Warning(Exception exception, string messageTemplate)
        {
            Log.Warning(exception, messageTemplate);
        }

        public static void Warning<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Warning<T>(exception, messageTemplate, propertyValue);
        }

        public static void Warning<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Warning<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Warning<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Warning<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Warning(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Warning(exception, messageTemplate, propertyValues);
        }
    }
}