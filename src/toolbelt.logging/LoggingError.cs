using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Error(string messageTemplate)
        {
            Log.Error(messageTemplate);
        }

        public static void Error<T>(string messageTemplate, T propertyValue)
        {
            Log.Error<T>(messageTemplate, propertyValue);
        }

        public static void Error<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Error<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Error<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Error<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Error(string messageTemplate, params object[] propertyValues)
        {
            Log.Error(messageTemplate, propertyValues);
        }

        public static void Error(Exception exception, string messageTemplate)
        {
            Log.Error(exception, messageTemplate);
        }

        public static void Error<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Error<T>(exception, messageTemplate, propertyValue);
        }

        public static void Error<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Error<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Error<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Error<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Error(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Error(exception, messageTemplate, propertyValues);
        }
    }
}