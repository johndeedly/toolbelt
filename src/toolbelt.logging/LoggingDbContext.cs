using System.IO;
using Microsoft.EntityFrameworkCore;

namespace toolbelt
{
    public class LoggingDbContext : PostgreSqlContext
    {
        public LoggingDbContext(string connectionString, string tableName = "Logs") : base(connectionString, tableName)
        {
            using (StreamReader sr = new StreamReader(typeof(LoggingDbContext).Assembly.GetManifestResourceStream("toolbelt.logging.TableLogsUp.sql")))
            {
                string content = sr.ReadToEnd();
                content = content.Replace("{{TABLE_NAME}}", base.tableName);
                Database.ExecuteSqlRaw(content);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LogEntry>().ToTable(tableName);
        }

        public DbSet<LogEntry> LogEntries { get; set; }
    }
}