using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;

namespace toolbelt
{
    public class PostgreSqlContext : DbContext
    {
        protected string connectionString;
        protected string tableName;
        protected static readonly Regex invalidTableNames = new Regex("[^a-zA-Z_0-9]");

        public PostgreSqlContext(string connectionString, string tableName)
        {
            if (invalidTableNames.IsMatch(tableName))
                throw new SqlInjectionAttemptException();
            this.connectionString = connectionString;
            this.tableName = tableName;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(connectionString);
        }
    }
}