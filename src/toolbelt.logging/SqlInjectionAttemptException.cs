using System;
using System.Runtime.Serialization;

namespace toolbelt
{
    [Serializable]
    public class SqlInjectionAttemptException : Exception
    {
        public SqlInjectionAttemptException()
        { }

        public SqlInjectionAttemptException(string message) : base(message)
        { }

        public SqlInjectionAttemptException(string message, Exception innerException) : base(message, innerException)
        { }

        protected SqlInjectionAttemptException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }
    }
}