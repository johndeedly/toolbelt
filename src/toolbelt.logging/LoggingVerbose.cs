using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Verbose(string messageTemplate)
        {
            Log.Verbose(messageTemplate);
        }

        public static void Verbose<T>(string messageTemplate, T propertyValue)
        {
            Log.Verbose<T>(messageTemplate, propertyValue);
        }

        public static void Verbose<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Verbose<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Verbose<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Verbose<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Verbose(string messageTemplate, params object[] propertyValues)
        {
            Log.Verbose(messageTemplate, propertyValues);
        }

        public static void Verbose(Exception exception, string messageTemplate)
        {
            Log.Verbose(exception, messageTemplate);
        }

        public static void Verbose<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Verbose<T>(exception, messageTemplate, propertyValue);
        }

        public static void Verbose<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Verbose<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Verbose<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Verbose<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Verbose(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Verbose(exception, messageTemplate, propertyValues);
        }
    }
}