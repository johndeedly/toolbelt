using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Information(string messageTemplate)
        {
            Log.Information(messageTemplate);
        }

        public static void Information<T>(string messageTemplate, T propertyValue)
        {
            Log.Information<T>(messageTemplate, propertyValue);
        }

        public static void Information<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Information<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Information<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Information<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Information(string messageTemplate, params object[] propertyValues)
        {
            Log.Information(messageTemplate, propertyValues);
        }

        public static void Information(Exception exception, string messageTemplate)
        {
            Log.Information(exception, messageTemplate);
        }

        public static void Information<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Information<T>(exception, messageTemplate, propertyValue);
        }

        public static void Information<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Information<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Information<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Information<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Information(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Information(exception, messageTemplate, propertyValues);
        }
    }
}