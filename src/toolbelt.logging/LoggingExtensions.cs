using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static IHostBuilder AddToolbeltLogging(this IHostBuilder builder)
        {
            return builder.UseSerilog();
        }
        
        public static IApplicationBuilder AddToolbeltRequestLogging(this IApplicationBuilder app)
        {
            return app.UseSerilogRequestLogging();
        }
        
        public static ILoggingBuilder AddToolbeltLogging(this ILoggingBuilder builder)
        {
            return builder.AddSerilog();
        }

        public static ILoggerFactory AddToolbeltLogging(this ILoggerFactory factory)
        {
            return factory.AddSerilog();
        }
    }
}