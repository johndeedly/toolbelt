CREATE TABLE IF NOT EXISTS "{{TABLE_NAME}}" (
  "Message" text NULL,
  "MessageTemplate" text NULL,
  "Level" varchar NULL,
  "RaiseDate" timestamptz NULL,
  "Exception" text NULL,
  "LogEvent" jsonb NULL,
  "Properties" jsonb NULL
);

CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_Message_idx" ON "{{TABLE_NAME}}" USING GIN ("Message" gin_trgm_ops);
CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_Level_idx" ON "{{TABLE_NAME}}" ("Level");
CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_RaiseDate_idx" ON "{{TABLE_NAME}}" ("RaiseDate");
CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_Exception_idx" ON "{{TABLE_NAME}}" USING GIN ("Exception" gin_trgm_ops);
CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_LogEvent_idx" ON "{{TABLE_NAME}}" USING GIN ("LogEvent" jsonb_path_ops);
CREATE INDEX IF NOT EXISTS "{{TABLE_NAME}}_Properties_idx" ON "{{TABLE_NAME}}" USING GIN ("Properties" jsonb_path_ops);
