using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Fatal(string messageTemplate)
        {
            Log.Fatal(messageTemplate);
        }

        public static void Fatal<T>(string messageTemplate, T propertyValue)
        {
            Log.Fatal<T>(messageTemplate, propertyValue);
        }

        public static void Fatal<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Fatal<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Fatal<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Fatal<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Fatal(string messageTemplate, params object[] propertyValues)
        {
            Log.Fatal(messageTemplate, propertyValues);
        }

        public static void Fatal(Exception exception, string messageTemplate)
        {
            Log.Fatal(exception, messageTemplate);
        }

        public static void Fatal<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Fatal<T>(exception, messageTemplate, propertyValue);
        }

        public static void Fatal<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Fatal<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Fatal<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Fatal<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Fatal(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Fatal(exception, messageTemplate, propertyValues);
        }
    }
}