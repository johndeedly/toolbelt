using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.Threading;
using NpgsqlTypes;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.PostgreSQL.ColumnWriters;
using Serilog.Sinks.SystemConsole.Themes;

namespace toolbelt
{
    public enum LoggingLevel
    {
        Verbose,
        Debug,
        Information,
        Warning,
        Error,
        Fatal
    }

    public static partial class Logging
    {
        public static bool WriteToConsole { get; set; } = true;
        public static LoggingLevel ConsoleMinimumLevel { get; set; } = LoggingLevel.Information;
        public static LoggingLevel ConsoleMinimumLevelStandardError { get; set; } = LoggingLevel.Error;
        public static bool WriteToFile { get; set; } = false;
        public static LoggingLevel FileMinimumLevel { get; set; } = LoggingLevel.Information;
        public static string FileName { get; set; } = $"{Assembly.GetEntryAssembly().GetName().Name}.log";
        public static bool WriteToPostgreSQL { get; set; } = false;
        public static LoggingDbContext DbContext { get; set; } = null;
        public static LoggingLevel PostgreSQLMinimumLevel { get; set; } = LoggingLevel.Information;
        public static string ConnectionString { get; set; } = string.Empty;
        public static string TableName { get; set; } = string.Empty;
        public static bool WriteToEmail { get; set; } = false;
        public static LoggingLevel EmailMinimumLevel { get; set; } = LoggingLevel.Information;
        public static string FromEmail { get; set; } = string.Empty;
        public static string[] ToEmails { get; set; } = new string[0];
        public static string MailServer { get; set; } = string.Empty;
        public static ICredentialsByHost MailServerCredential { get; set; } = null;
        public static int MailPostingBatchLimit { get; set; } = 1000;
        public static TimeSpan MailPostingPeriod { get; set; } = TimeSpan.FromMinutes(15);
        public static bool WriteToSyslog { get; set; } = false;
        public static LoggingLevel SyslogMinimumLevel { get; set; } = LoggingLevel.Information;
        public static IPEndPoint SyslogTcpEndpoint { get; set; }
        public static IPEndPoint SyslogUdpEndpoint { get; set; }
        public static string SyslogAppName { get; set; } = Assembly.GetEntryAssembly().GetName().Name;

        public static LoggingLevel MinimumLoggingLevel
        {
            get
            {
                return ConvertLogEventLevel(_loggingLevelSwitch.MinimumLevel);
            }
            set
            {
                _loggingLevelSwitch.MinimumLevel = ConvertLogLevel(value);
            }
        }
        private static LoggingLevelSwitch _loggingLevelSwitch = new LoggingLevelSwitch(LogEventLevel.Information);

        public static LoggingLevel MinimumLoggingLevelMicrosoft
        {
            get
            {
                return ConvertLogEventLevel(_loggingLevelSwitchMicrosoft.MinimumLevel);
            }
            set
            {
                _loggingLevelSwitchMicrosoft.MinimumLevel = ConvertLogLevel(value);
            }
        }
        private static LoggingLevelSwitch _loggingLevelSwitchMicrosoft = new LoggingLevelSwitch(LogEventLevel.Warning);

        public static LoggingLevel MinimumLoggingLevelMicrosoftAspNetCore
        {
            get
            {
                return ConvertLogEventLevel(_loggingLevelSwitchMicrosoftAspNetCore.MinimumLevel);
            }
            set
            {
                _loggingLevelSwitchMicrosoftAspNetCore.MinimumLevel = ConvertLogLevel(value);
            }
        }
        private static LoggingLevelSwitch _loggingLevelSwitchMicrosoftAspNetCore = new LoggingLevelSwitch(LogEventLevel.Warning);

        public static bool Initialize()
        {
            try
            {
                LoggerConfiguration conf = new LoggerConfiguration();
                conf.MinimumLevel.ControlledBy(_loggingLevelSwitch);
                conf.MinimumLevel.Override("Microsoft", _loggingLevelSwitchMicrosoft);
                conf.MinimumLevel.Override("Microsoft.AspNetCore", _loggingLevelSwitchMicrosoftAspNetCore);
                conf.Enrich.FromLogContext();
                if (WriteToConsole)
                {
                    LogEventLevel lvl = ConvertLogLevel(ConsoleMinimumLevel);
                    LogEventLevel lvlErr = ConvertLogLevel(ConsoleMinimumLevelStandardError);
                    if (OperatingSystem.IsLinux() || (OperatingSystem.IsWindows() && OperatingSystem.IsWindowsVersionAtLeast(10)))
                    {
                        conf.WriteTo.Console(restrictedToMinimumLevel: lvl,
                            standardErrorFromLevel: lvlErr,
                            theme: AnsiConsoleTheme.Sixteen,
                            applyThemeToRedirectedOutput: true);
                    }
                    else
                    {
                        conf.WriteTo.Console(restrictedToMinimumLevel: lvl,
                            standardErrorFromLevel: lvlErr,
                            theme: SystemConsoleTheme.Colored,
                            applyThemeToRedirectedOutput: true);
                    }
                }
                if (WriteToFile)
                {
                    LogEventLevel lvl = ConvertLogLevel(FileMinimumLevel);
                    conf.WriteTo.File(FileName,
                        restrictedToMinimumLevel: lvl,
                        rollingInterval: RollingInterval.Day,
                        rollOnFileSizeLimit: true);
                }
                if (WriteToPostgreSQL)
                {
                    DbContext = new LoggingDbContext(ConnectionString, TableName);
                    LogEventLevel lvl = ConvertLogLevel(PostgreSQLMinimumLevel);
                    IDictionary<string, ColumnWriterBase> columnWriters = new Dictionary<string, ColumnWriterBase>
                    {
                        { "Message", new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
                        { "MessageTemplate", new MessageTemplateColumnWriter(NpgsqlDbType.Text) },
                        { "Level", new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
                        { "RaiseDate", new TimestampColumnWriter(NpgsqlDbType.TimestampTz) },
                        { "Exception", new ExceptionColumnWriter(NpgsqlDbType.Text) },
                        { "LogEvent", new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
                        { "Properties", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) }
                    };
                    conf.WriteTo.PostgreSQL(ConnectionString, TableName,
                        columnOptions: columnWriters,
                        restrictedToMinimumLevel: lvl);
                }
                if (WriteToEmail)
                {
                    LogEventLevel lvl = ConvertLogLevel(EmailMinimumLevel);
                    if (MailPostingPeriod < TimeSpan.FromMinutes(15))
                        MailPostingPeriod = TimeSpan.FromMinutes(15);
                    if (MailPostingBatchLimit > 10000)
                        MailPostingBatchLimit = 10000;
                    if (MailPostingBatchLimit < 100)
                        MailPostingBatchLimit = 100;
                    conf.WriteTo.Email(FromEmail, ToEmails, MailServer,
                        networkCredential: MailServerCredential,
                        restrictedToMinimumLevel: lvl,
                        batchPostingLimit: MailPostingBatchLimit,
                        period: MailPostingPeriod);
                }
                if (WriteToSyslog)
                {
                    LogEventLevel lvl = ConvertLogLevel(SyslogMinimumLevel);
                    if (SyslogUdpEndpoint != null)
                    {
                        string remote = SyslogUdpEndpoint.Address.ToString();
                        int port = SyslogUdpEndpoint.Port;
                        if (port == 0)
                            port = 514;
                        conf.WriteTo.UdpSyslog(remote, port: port,
                            appName: SyslogAppName, restrictedToMinimumLevel: lvl);
                    }
                    if (SyslogTcpEndpoint != null)
                    {
                        string remote = SyslogTcpEndpoint.Address.ToString();
                        int port = SyslogTcpEndpoint.Port;
                        if (port == 0)
                            port = 514;
                        conf.WriteTo.TcpSyslog(remote, port: port,
                            appName: SyslogAppName, restrictedToMinimumLevel: lvl);
                    }
                    if (OperatingSystem.IsLinux())
                    {
                        conf.WriteTo.LocalSyslog(appName: SyslogAppName, restrictedToMinimumLevel: lvl);
                    }
                }
                Log.Logger = conf.CreateLogger();
                AppDomain dom = AppDomain.CurrentDomain;
                dom.UnhandledException += (o, e) =>
                {
                    Exception ex = e.ExceptionObject as Exception;
                    Logging.Fatal(ex, "Unhandled exception logged");
                    CloseAndFlush();
                    if (!Debugger.IsAttached)
                        Environment.Exit(System.Runtime.InteropServices.Marshal.GetHRForException(ex));
                };
                Thread t = new Thread(CloseAndFlushHiddenThread);
                t.Name = "CloseAndFlush";
                t.Start(Thread.CurrentThread);
                return true;
            }
            catch (Exception)
            {
                CloseAndFlush();
                return false;
            }
        }

        static void CloseAndFlushHiddenThread(object o)
        {
            Thread main = (Thread)o;
            bool exit = false;
            while (!exit && main.IsAlive)
                exit = main.Join(727);
            CloseAndFlush();
        }

        public static void CloseAndFlush()
        {
            Log.CloseAndFlush();
            IDisposable log = ((object)Log.Logger) as IDisposable;
            log?.Dispose();
            DbContext?.Dispose();
            DbContext = null;
        }

        private static LogEventLevel ConvertLogLevel(LoggingLevel level)
        {
            LogEventLevel lvl = LogEventLevel.Information;
            switch (level)
            {
                case LoggingLevel.Verbose:
                    lvl = LogEventLevel.Verbose;
                    break;
                case LoggingLevel.Debug:
                    lvl = LogEventLevel.Debug;
                    break;
                case LoggingLevel.Information:
                    lvl = LogEventLevel.Information;
                    break;
                case LoggingLevel.Warning:
                    lvl = LogEventLevel.Warning;
                    break;
                case LoggingLevel.Error:
                    lvl = LogEventLevel.Error;
                    break;
                case LoggingLevel.Fatal:
                    lvl = LogEventLevel.Fatal;
                    break;
            }
            return lvl;
        }

        private static LoggingLevel ConvertLogEventLevel(LogEventLevel level)
        {
            LoggingLevel lvl = LoggingLevel.Information;
            switch (level)
            {
                case LogEventLevel.Verbose:
                    lvl = LoggingLevel.Verbose;
                    break;
                case LogEventLevel.Debug:
                    lvl = LoggingLevel.Debug;
                    break;
                case LogEventLevel.Information:
                    lvl = LoggingLevel.Information;
                    break;
                case LogEventLevel.Warning:
                    lvl = LoggingLevel.Warning;
                    break;
                case LogEventLevel.Error:
                    lvl = LoggingLevel.Error;
                    break;
                case LogEventLevel.Fatal:
                    lvl = LoggingLevel.Fatal;
                    break;
            }
            return lvl;
        }
    }
}