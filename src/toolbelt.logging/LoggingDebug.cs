using System;
using Serilog;

namespace toolbelt
{
    public static partial class Logging
    {
        public static void Debug(string messageTemplate)
        {
            Log.Debug(messageTemplate);
        }

        public static void Debug<T>(string messageTemplate, T propertyValue)
        {
            Log.Debug<T>(messageTemplate, propertyValue);
        }

        public static void Debug<T0, T1>(string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Debug<T0, T1>(messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Debug<T0, T1, T2>(string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Debug<T0, T1, T2>(messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Debug(string messageTemplate, params object[] propertyValues)
        {
            Log.Debug(messageTemplate, propertyValues);
        }

        public static void Debug(Exception exception, string messageTemplate)
        {
            Log.Debug(exception, messageTemplate);
        }

        public static void Debug<T>(Exception exception, string messageTemplate, T propertyValue)
        {
            Log.Debug<T>(exception, messageTemplate, propertyValue);
        }

        public static void Debug<T0, T1>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1)
        {
            Log.Debug<T0, T1>(exception, messageTemplate, propertyValue0, propertyValue1);
        }

        public static void Debug<T0, T1, T2>(Exception exception, string messageTemplate, T0 propertyValue0, T1 propertyValue1, T2 propertyValue2)
        {
            Log.Debug<T0, T1, T2>(exception, messageTemplate, propertyValue0, propertyValue1, propertyValue2);
        }

        public static void Debug(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            Log.Debug(exception, messageTemplate, propertyValues);
        }
    }
}