using System;
using System.Collections.Generic;
using Serilog.Context;

namespace toolbelt
{
    public class LoggingContext : IDisposable
    {
        List<IDisposable> contexts;

        public LoggingContext()
        {
            contexts = new List<IDisposable>();
        }

        public void PushProperty(string name, object value, bool destructureObjects = false)
        {
            contexts.Add(LogContext.PushProperty(name, value, destructureObjects));
        }

        public void Dispose()
        {
            if (contexts != null)
            {
                foreach (var context in contexts)
                {
                    context?.Dispose();
                }
                contexts.Clear();
            }
            contexts = null;
        }
    }
}