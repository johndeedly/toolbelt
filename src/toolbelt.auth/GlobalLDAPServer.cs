using Novell.Directory.Ldap;

namespace toolbelt
{
    public static class GlobalLDAPServer
    {
        public static string UsersGroupDIT { get; set; }

        public static string LdapServer { get; set; }

        public static int LdapPort { get; set; } = LdapConnection.DefaultPort;
    }
}
