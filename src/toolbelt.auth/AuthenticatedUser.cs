using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace toolbelt
{
    public class AuthenticatedUser : ClaimsIdentity
    {
        public AuthenticatedUser(string authenticationType, string name, IEnumerable<Claim> claims) : base(new[] {
            new Claim(ClaimTypes.Name, name)
        }.Concat(claims).ToArray(), authenticationType)
        { }
    }
}
