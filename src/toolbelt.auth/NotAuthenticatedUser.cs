using System.Security.Claims;
using System.Security.Principal;

namespace toolbelt
{
    public class NotAuthenticatedUser : ClaimsIdentity
    {
        public NotAuthenticatedUser(string name) : base(new[] {
            new Claim(ClaimTypes.Name, name)
        })
        { }
    }
}
