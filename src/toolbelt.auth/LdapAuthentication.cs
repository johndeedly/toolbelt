using System.Collections.Generic;
using System.Security.Claims;
using Novell.Directory.Ldap;

namespace toolbelt
{
    public static class LdapAuthentication
    {
        /// var loggedInUser = LdapAuthentication.AuthenticateUser("user", "resu", "cn=users,dc=local,dc=domain", "local.domain");
        /// foreach (var claim in loggedInUser.Claims)
        /// {
        ///     Logging.Information("{Type}: {Value}", claim.Type, claim.Value);
        /// }
        
        public static ClaimsPrincipal AuthenticateUser(string username, string password)
        {
            return AuthenticateUser(username, password, GlobalLDAPServer.UsersGroupDIT, GlobalLDAPServer.LdapServer, GlobalLDAPServer.LdapPort);
        }
        
        public static ClaimsPrincipal AuthenticateUser(string username, string password, string usersGroupDIT, string ldapServer, int ldapPort = LdapConnection.DefaultPort)
        {
            using (var lc = new LoggingContext())
            {
                string fqusername = $"uid={username},{usersGroupDIT}";
                string fqsearch = $"(uid={username})";
                
                lc.PushProperty("Username", username);
                lc.PushProperty("FQDN", fqusername);
                lc.PushProperty("Server", ldapServer);
                lc.PushProperty("Port", ldapPort);
                
                try
                {
                    using (var cn = new LdapConnection())
                    {
                        cn.Connect(ldapServer, ldapPort);
                        cn.Bind(LdapConnection.LdapV3, fqusername, password);

                        LdapSearchQueue sq = cn.Search(
                            usersGroupDIT,
                            LdapConnection.ScopeSub,
                            fqsearch,
                            new[] { LdapConnection.AllUserAttrs },
                            false,
                            null as LdapSearchQueue);

                        List<Claim> claims = new();
                        LdapMessage message;
                        while ((message = sq.GetResponse()) != null)
                        {
                            if (!(message is LdapSearchResult result))
                            {
                                continue;
                            }
                            LdapEntry entry = result.Entry;
                            LdapAttributeSet set = entry.GetAttributeSet();

                            foreach (var key in set.Keys)
                            {
                                LdapAttribute attr = set.GetAttribute(key);
                                string[] vals = attr.StringValueArray;
                                foreach (var val in vals)
                                {
                                    Claim claim = new Claim(attr.Name, val);
                                    claims.Add(claim);
                                }
                            }
                        }
                        
                        lc.PushProperty("Claims", claims);
                        Logging.Information("LDAP {User} authenticated.", fqusername);

                        AuthenticatedUser user = new AuthenticatedUser("LDAP", fqusername, claims);
                        var claimsPrincipal = new ClaimsPrincipal(user);
                        return claimsPrincipal;
                    }
                }
                catch (LdapException ex)
                {
                    Logging.Error(ex, "LDAP {User} authentication failed.", fqusername);

                    NotAuthenticatedUser user = new NotAuthenticatedUser(fqusername);
                    var claimsPrincipal = new ClaimsPrincipal(user);
                    return claimsPrincipal;
                }
            }
        }
    }
}