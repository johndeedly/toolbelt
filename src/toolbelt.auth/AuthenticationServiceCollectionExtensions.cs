using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

namespace toolbelt
{
    public static class AuthenticationServiceCollectionExtensions
    {
        public static IServiceCollection AddBasicToLDAPAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication().AddScheme<AuthenticationSchemeOptions, BasicToLDAPAuthenticationHandler>(BasicToLDAPAuthorizationAttribute.PolicyName, options => { });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(BasicToLDAPAuthorizationAttribute.PolicyName, new AuthorizationPolicyBuilder(BasicToLDAPAuthorizationAttribute.PolicyName).RequireAuthenticatedUser().Build());
            });
            return services;
        }
    }
}