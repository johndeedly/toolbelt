using Microsoft.AspNetCore.Authorization;

namespace toolbelt
{
    public class BasicToLDAPAuthorizationAttribute : AuthorizeAttribute
    {
        public static readonly string PolicyName = "BasicToLDAPAuthentication";

        public BasicToLDAPAuthorizationAttribute()
        {
            Policy = PolicyName;
        }
    }
}
